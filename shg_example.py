# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

def ms1(sample, y=False, x=False, y45=False, x45=False, scan=False):
    def ms_sw():
        #sm.polarization_control_motor.set_position(0)     #y
        #sm.polarization_control_motor.set_position(45)    #x
        #sm.polarization_control_motor.set_position(-22.5) #y+45 
        #sm.polarization_control_motor.set_position(22.5)  #x+45
        wl_range = fp.wnr(50)
        
        if y45 and ph.stopper.is_running():
            sm.polarization_control_motor.set_position(-22.5) #y+45 
            sm.measure_wavelengths(sample + "_xin_yout_8mw_1000-1300nm_1s_3r_770.wl", wl_range, 3)
            
        if y and ph.stopper.is_running():
            sm.polarization_control_motor.set_position(0)     #y
            sm.measure_wavelengths(sample + "_yin_yout_1000-1300nm_1s_3r_770.wl", wl_range, 3)
            
        if x45 and ph.stopper.is_running():    
            sm.polarization_control_motor.set_position(22.5)  #x+45
            sm.measure_wavelengths(sample + "_yin_yout_8mw_1000-1300nm_1s_3r_770.wl", wl_range, 3)
    
        if x and ph.stopper.is_running():
            sm.polarization_control_motor.set_position(45)    #x
            sm.measure_wavelengths(sample + "_xin_yout_1000-1300nm_1s_3r_770.wl", wl_range, 3)
        
        if scan and ph.stopper.is_running():
            sm.polarization_control_motor.set_position(0, wait=False)     #y
            sm.measure_wavelengths_hwp_angle("lr_" + sample + "_yin_yout.wla",
                                             WavelengthRange([1060]),
                                             list(np.linspace(0,360,36*4+1)) )
        
        sm.polarization_control_motor.set_position(0, wait=False)     #y
        sm.power_control.set_wavelength_nm(1030, wait=False)   #y
    ph.stopper.start_function(ms_sw)

def ms_fibers(sample):
    #sm.polarization_control_motor.set_position(0)     #y
    #sm.polarization_control_motor.set_position(45)    #x
    #sm.polarization_control_motor.set_position(-22.5) #y+45 
    #sm.polarization_control_motor.set_position(22.5)  #x+45
    
    sm.polarization_control_motor.set_position(0)     #y
    sm.measure_wavelengths(sample + "_yin_allout_1000-1300nm_1s_3r_770.wl", fp.wnr(40), 3)

    sm.polarization_control_motor.set_position(45)    #x
    sm.measure_wavelengths(sample + "_xin_allout_1000-1300nm_1s_3r_770.wl", fp.wnr(40), 3)
    
    sm.polarization_control_motor.set_position(25)     #y
    sm.measure_wavelengths(sample + "_25in_allout_1000-1300nm_1s_3r_770.wl", fp.wnr(40), 3)

    sm.polarization_control_motor.set_position(70)    #x
    sm.measure_wavelengths(sample + "_70in_allout_1000-1300nm_1s_3r_770.wl", fp.wnr(40), 3)

    sm.polarization_control_motor.set_position(0, wait=False)     #y
    sm.measure_wavelengths_hwp_angle(sample + "_yin_yout.wla",
                                     fp.wnr(11),
                                     list(np.linspace(0,180,18*4+1)) )
    
    sm.power_control.set_wavelength_nm(1030, wait=False)
    sm.polarization_control_motor.set_position(0, wait=False)     #y

def msp(sample):
    def ms_sw():
        wl_range = fp.wnr(50)
        
        for p in (5, 10, 20, 40):
            if not ph.stopper.is_running():
                break
            sm.power_control.set_target_power_mw(p)
            sm.measure_wavelengths(sample + "_" + str(p) + "mw_1000-1300nm_1s_3r_770.wl", wl_range, 3)
        
        sm.power_control.set_wavelength_nm(1030, wait=False)   #y
    ph.stopper.start_function(ms_sw)
