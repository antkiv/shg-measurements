# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import photonics as ph

from photonics.core.stopper import *
from shg_measurements.shg_measurements import ShgMeasurements
from shg_measurements import fast_plot as fp


def init():
    """
    Initialize the measurement system.
    Creates global ShgMeasurements object sm.
    """
    global sm
    
    if 'sm' in globals():
        sm.terminate()
    sm = ShgMeasurements()

    sm.initialize(opo_port = "/dev/ttyUSB1",
                  motor_controller_port = "/dev/ttyUSB0",
                  arduino_port = "/dev/ttyACM0",
                  power_meter_ophir_port = "/dev/ttyUSB3",
                  power_meter_thorlabs = False,
                  photon_counter_port = "/dev/pts/0",
                  power_calibration_file = "../power_calibrations/20190905.ipc")

    # Start bluetooth server.
    ph.stopper.start_bt_server()

def initt():
    """
    Init function for testing without measurement hardware.
    """
    global sm
    
    if 'sm' in globals():
        sm.terminate()
    sm = ShgMeasurements()

    sm.initialize(opo_port=None,
                  motor_controller_port=None,
                  arduino_port=None,
                  power_meter_ophir_port=None,
                  power_meter_thorlabs=None,
                  photon_counter_port=None,
                  power_calibration_file=None)
    
def term():
    """
    Termination function for the measurement system. Call when you no longer use the system.
    """
    if "sm" in globals():
        sm.terminate()
    

###################
# Below are quick convenience functions for easy usage in IPython.
    

def m():
    sm.measure(True, False, False)
def mb():
    print("Diode: " + str(sm.power_control.reference_diode.get_voltage()))
def mc():
    def func():
        while ph.stopper.is_running():
            sm.measure(True, False, False)
    ph.stopper.start_function(func)
def pmsp():
    sm.photon_counter.print_parameters()
def opop():
    if sm.power_control.power_adjustment_loop_run == False:
        sm.opo.update_parameters()
        sm.opo.print_parameters()
    else:
        sm.opo.print_parameters()
def pp():
    sm.motor[0].print_position()
    sm.motor[1].print_position()
def oo():
    sm.power_control.set_opo_shutter(True)
def oc():
    sm.power_control.set_opo_shutter(False)
def po():
    sm.arduino.set_pmt_shutter_open(True)
def pc():
    sm.arduino.set_pmt_shutter_open(False)
def spal():
    sm.power_control.start_power_adjustment_loop()
def epal():
    sm.power_control.stop_power_adjustment_loop()
def mu():
    sm.arduino.mirror_up()
def md():
    sm.arduino.mirror_down()
def mr():
    sm.arduino.reset_mirror()

def smt(ip1):
    try:
        value = float(ip1)
    except ValueError:
        print(ip1, "is not a number")
    except IndexError:
        print("Please supply a value.")
    sm.photon_counter.set_measurement_time(value)
def sav(ip1):
    try:
        value = int(ip1)
    except ValueError:
        print(ip1, "is not a number")
    except IndexError:
        print("Please supply a value.")
    sm.photon_counter.set_averaging(value)
def swl(ip1):
    try:
        wavelength = float(ip1)
    except Exception:
        print("Please supply wavelength in nm.")
    sm.power_control.set_wavelength_nm(wavelength, wait=False)
def sp(ip1):
    try:
        power = float(ip1)
    except Exception:
        print("Please supply power in mW.")
    sm.power_control.set_target_power_mw(power)
def spol(ip1):
    try:
        polarization = float(ip1)
    except Exception:
        print("Please supply polarization in degrees.")
    sm.polarization_control_motor.set_position(polarization, wait=False)
def spo(ip1):
    try:
        power = float(ip1)
    except Exception:
        print("Please supply power in mW.")
    sm.power_control.set_target_power_from_calibration_mw(power)
def spid(ip1, ip2):
    try:
        p = float(ip1)
        i = float(ip2)
    except Exception:
        print("Please supply p and i.")
    sm.power_control.pid_p = p
    sm.power_control.pid_i = i
def pr(ip1, motor=2):
    try:
        value = float(ip1)
    except ValueError:
        print(ip1, "is not a number")
    except IndexError:
        print("Please supply a value.")
    sm.motor[motor-1].set_position(value, relative=True, wait=False)
def pa(ip1, motor=2):
    try:
        value = float(ip1)
    except ValueError:
        print(ip1, "is not a number")
    except IndexError:
        print("Please supply a value.")
    sm.motor[motor-1].set_position(value, wait=False)

def ws(motor=2):
    sm.motor[motor-1].wait_to_stop()
    print("Motor " + str(motor) + " stopped")
