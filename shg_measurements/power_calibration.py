# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import time

import numpy as np
from scipy import interpolate

from .measurement import Measurement
from photonics.devices.power_meter import INVALID_POWER
import photonics as ph

class PowerCalibration():
    def __init__(self, calibration_file):
        self.calibration_measurement = None
        self.load_calibration(calibration_file)
        
    def load_calibration(self, calibration_file):
        """
        Loads a power calibration measurement object from a csv file.
        """
        self.calibration_measurement = Measurement()
        self.calibration_measurement.load(calibration_file)

        self.wavelength_index = self.calibration_measurement.get_label_index(
                                    "Wavelength (nm)")
        self.hwp_angle_index = self.calibration_measurement.get_label_index(
                                    "HWP angle (degrees)")
        self.meter_power_index = self.calibration_measurement.get_label_index(
                                    "Calibration power meter power (W)")
        self.opo_power_index = self.calibration_measurement.get_label_index(
                                    "Opo power (W)")
        self.diode_voltage_index = self.calibration_measurement.get_label_index(
                                    "Reference diode voltage (V)")
        
        # 2D array containing only wavelength and power meter power columns.
        wl_power   = self.calibration_measurement.points[:,
                            [self.wavelength_index, self.meter_power_index]]
        # 2D array containing only wavelength and photodiode voltage columns.
        wl_voltage = self.calibration_measurement.points[:,
                            [self.wavelength_index, self.diode_voltage_index]]
        # 1D arrays.
        hwp_angle  = self.calibration_measurement.points[:, self.hwp_angle_index]
        power      = self.calibration_measurement.points[:, self.meter_power_index]
        opo_power  = self.calibration_measurement.points[:, self.opo_power_index]
        voltage    = self.calibration_measurement.points[:, self.diode_voltage_index] 
    
        # 2D interpolators using wavelenth and power as input.
        self.wl_power_to_hwp_angle_interp = \
            interpolate.LinearNDInterpolator(wl_power, hwp_angle)
        self.wl_power_to_opo_power_interp = \
            interpolate.LinearNDInterpolator(wl_power, opo_power)
        self.wl_power_to_diode_voltage_interp = \
            interpolate.LinearNDInterpolator(wl_power, voltage)
        
        # 2D interpolators using wavelenth and voltage as input.
        self.wl_voltage_to_hwp_angle_interp = \
            interpolate.LinearNDInterpolator(wl_voltage, hwp_angle)
        self.wl_voltage_to_power_interp     = \
            interpolate.LinearNDInterpolator(wl_voltage, power)
        self.wl_voltage_to_opo_power_interp = \
            interpolate.LinearNDInterpolator(wl_voltage, opo_power)
        
        logging.info("Power calibration: " + calibration_file + " loaded.")
        
    # Getter functions that use the 2D interpolators.
    def wl_voltage_get_hwp_angle(self, wavelength_nm, voltage):            
        return float(self.wl_voltage_to_hwp_angle_interp( (wavelength_nm, voltage) ))
        
    def wl_voltage_get_power(self, wavelength_nm, voltage):            
        return float(self.wl_voltage_to_power_interp    ( (wavelength_nm, voltage) ))
                
    def wl_voltage_get_opo_power(self, wavelength_nm, voltage):
        return float(self.wl_voltage_to_opo_power_interp( (wavelength_nm, voltage) ))
                
    # Getter functions that use the 2D interpolators.
    def wl_power_get_hwp_angle(self, wavelength_nm, power):            
        return float(self.wl_power_to_hwp_angle_interp    ( (wavelength_nm, power) ))
                
    def wl_power_get_opo_power(self, wavelength_nm, power):
        return float(self.wl_power_to_opo_power_interp    ( (wavelength_nm, power) ))
        
    def wl_power_get_voltage(self, wavelength_nm, power):            
        return float(self.wl_power_to_diode_voltage_interp( (wavelength_nm, power) ))



    def init_calibration_devices(self, opo, motor, power_meter, arduino, reference_diode, photon_counter):
        """
        This needs to be called before using calibrate_reference_powers().
        Currently this is done automatically in ShgMeasurements.
        """
        self.opo = opo
        self.motor = motor
        self.calibration_power_meter = power_meter
        self.arduino = arduino
        self.reference_diode = reference_diode
        self.photon_counter = photon_counter
        
    def calibrate_reference_powers(self, filename, angles, wavelength_range,
                                   pmt=False, power_meter=False, shg_power_meter=False, stop_limit=1.02):
        """
        Automated calibration function. Goes through all HWP angle and wavelength_range
        combinations and measures photodiode voltages and PMT counts or power.
        
        Call init_calibration_devices once before using this function.
        Currently this done automatically in ShgMeasurements.

        Example:
        sm.power_calibration.calibrate_reference_powers("calibration_file.ipc",
                                                        power_calibration_angles_lin(50),
                                                        fp.wnr(50,1000,1300),
                                                        pmt=False,
                                                        power_meter=True,
                                                        shg_power_meter=False)

        """
        ph.stopper.start_function(PowerCalibration.__calibrate_reference_powers_nostopper,
                                  (self, filename, angles, wavelength_range,
                                   pmt, power_meter, shg_power_meter, stop_limit))
        
    def __calibrate_reference_powers_nostopper(self, filename, angles, wavelength_range,
                                          pmt, power_meter, shg_power_meter, stop_limit):
        """
        Called by calibrate_reference_powers()
        """
        
        if power_meter and shg_power_meter:
            print("Can not use both power meters at the same time")
            return
            
        self.opo.set_shutter(True)
        if pmt:
            self.arduino.set_pmt_shutter_open(True)
            
        measurement = Measurement()                                
        measurement.set_labels_list(["Time (s)",
                                "Wavelength (nm)",
                                "Detector count",
                                "Reference Detector count",
                                "Calibration power meter power (W)",
                                "Reference diode set point voltage (V)",
                                "Reference diode voltage (V)",
                                "HWP angle (degrees)",
                                "Opo power (W)"])
        measurement.start()
        
        for current_wl in wavelength_range.wavelengths_nm():
            if not ph.stopper.is_running():
                break

            self.motor.set_position(angles[0], wait=False)

            if shg_power_meter == True:
                self.calibration_power_meter.set_wavelength_nm(current_wl * 0.5)
            elif power_meter:
                self.calibration_power_meter.set_wavelength_nm(current_wl)
                
            self.opo.set_wavelength_nm(current_wl)
            self.opo.wait_status_ok()
            
            min_power = INVALID_POWER
            
            for current_angle in angles:
                if not ph.stopper.is_running():
                    break
                
                self.motor.set_position(current_angle)
                
                if shg_power_meter == True:
                    if pmt == True:
                        self.arduino.rotate_mirror_pmt(False)
                        time.sleep(2)
                elif power_meter:
                    if self.reference_diode.get_voltage() > 50:
                        continue
                
                current_power = 0.0
                if power_meter or shg_power_meter:
                    current_power = self.calibration_power_meter.get_power()
                
                if power_meter:
                    if current_power < min_power:
                        min_power = current_power
                    elif (current_power/min_power) > stop_limit:
                        break
                
                counts = (0, 0)
                if pmt == True:
                    if shg_power_meter == True:
                        self.arduino.rotate_mirror_pmt(True)
                        time.sleep(2)
                    counts = self.photon_counter.measure()
              
                current_reference_point = self.reference_diode.get_voltage()
                
                self.opo.update_parameters()
                current_opo_wavelength = self.opo.get_wavelength_nm()
                current_opo_power = self.opo.get_power()
                reference_set_point = 0
                current_read_angle = current_angle
                
                if power_meter:
                    if current_reference_point > 50:
                        continue
                
                measurement.add_point([measurement.get_time(),
                                       current_opo_wavelength,
                                       counts[1], counts[0],
                                       current_power,
                                       reference_set_point, current_reference_point,
                                       current_read_angle, current_opo_power],
                                       True)
        
        if pmt:
            self.arduino.set_pmt_shutter_open(False) 
            self.arduino.rotate_mirror_pmt(False)
        self.opo.set_shutter(False)
        
        measurement.end()
        measurement.save(filename)       
        measurement.plot(1,2,4)
        
        return measurement

def power_calibration_angles_lin(points = 101, start=0.0, end=1.0):
    """
    Returns HPW angles that should lead to linear power steps between start (min power=0.0)
    and end (max power is 1.0).
    """
    return list(np.arccos( np.sqrt(np.linspace(start,end,points)) ) * 0.5 * 180 / np.pi)


#power_calibration_angles1 = list(np.linspace(-40, -22, 10)) +\
#                            list(np.linspace(-20, -11, 10)) +\
#                            list(np.linspace(-10, -5.5, 10)) +\
#                            list(np.linspace(-5, 5, 41))
#power_calibration_angles2 = list(np.linspace(-30, -10.5, 40)) +\
#                            list(np.linspace(-10, 5, 16))