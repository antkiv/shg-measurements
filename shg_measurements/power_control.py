# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import time
import logging
import threading
import numpy as np

import photonics as ph

class PowerControl:
    """
    Controls and keeps the laser power stable at the sample plane.
    """
    
    def __init__(self, motor, opo, reference_diode, power_calibration, initial_power = 0.008):
        self.motor              = motor
        self.opo                = opo
        self.reference_diode    = reference_diode
        self.power_calibration  = power_calibration
        
        if opo:
            self.current_set_wavelength = self.opo.get_target_wavelength_nm()
        else:
            self.current_set_wavelength = 1060
            
        self.target_power               = initial_power
        self.current_read_angle         = 0
        self.current_reference_point    = 0
        self.reference_set_point        = 0
        self.wl_adjust_time             = -1
        
        self.pid_p = 1.0
        self.pid_i = 0.0
        self.pid_d = 0.0
        
        self.power_adjustment_loop_time = 0.012
        self.max_relative_error         = 0.015
        self.power_stable               = False
        self.adjust_wavelength          = False
        self.open_opo_shutter           = False
        self.close_opo_shutter          = False
        self.power_adjustment_loop_run  = False
        
    
    def get_target_power_mw(self):
        return self.target_power * 1000
        
    def set_opo_shutter(self, set_open=False):
        """
        Opo shutter shoud be controlled through this function especially if
        power adjustment loop is running.
        """
        if self.power_adjustment_loop_run:
            if set_open == True:
                self.open_opo_shutter  = True
                self.close_opo_shutter = False
            else:
                self.close_opo_shutter = True
                self.open_opo_shutter  = False
        else:
            self.opo.set_shutter(set_open)
            self.open_opo_shutter  = False
            self.close_opo_shutter = False
                  
    def set_wavelength_nm(self, wavelength_nm, wait=True):
        self.current_set_wavelength = wavelength_nm
        
        if self.power_adjustment_loop_run == True:
            self.adjust_wavelength = True
            if wait:
                time.sleep(1)
        else:
            if wait:
                self.opo.set_shutter(False)
                self.opo.set_wavelength_nm(self.current_set_wavelength)
                #self.set_target_power_from_calibration_mw()
                self.opo.wait_status_ok()
                self.opo.set_shutter(True)
            else:
                self.opo.set_wavelength_nm(self.current_set_wavelength)
                #self.set_target_power_from_calibration_mw()

    def set_target_power_mw(self, power_mw):
        self.target_power = power_mw * 0.001

    def set_target_power_from_calibration_mw(self, power_mw=None):
        """
        Tries to set the power without usign the photodiode reading.
        Does not work well. Not used.
        """
        if power_mw:
            self.target_power = power_mw * 0.001
            
        if self.power_adjustment_loop_run == False:            
            self.opo.update_parameters()
            
        try:
            opo_power = self.power_calibration.wl_power_get_opo_power(
                self.opo.get_wavelength_nm(), self.target_power)
                            
            corrected_power = opo_power / self.opo.get_power() * self.target_power

            new_position = self.power_calibration.wl_power_get_hwp_angle(
                self.opo.get_wavelength_nm(), corrected_power)
                
            if (new_position != np.nan):
                self.motor.set_position(new_position)            
            else:
                logging.warning("Requested opo power: " + str(self.target_power) +
                            "could not be set.")
        except Exception:
            logging.warning("Requested opo power: " + str(self.target_power) +
                            "could not be set.")

    def get_power_stable(self):
        """
        Returns True if the power is stable.
        """
        return self.power_stable
        
    def wait_power_stable(self):
        """
        Waits until the power is stable.
        """
        time.sleep(0.01)
        while self.power_stable == False:
            if ph.stopper.is_running() == False:
                break
            time.sleep(0.01)
            
    def min_power_angle_at_wl(self, wavelength):
        """
        Returns the HWP angle at which minimum power is reached.
        """
        #interpolate.NearestNDInterpolator(points, values)[source]
        return 1
        
    def max_power_angle_at_wl(self, wavelength):
        """
        Returns the HWP angle at which maximum power is reached.
        """
        return -45

    def get_loop_parameters(self):
        if self.power_adjustment_loop_run:
            return (self.opo.get_wavelength_nm(), self.opo.get_power(),
                    self.reference_set_point, self.current_reference_point,
                    self.current_read_angle)
        else:
            self.opo.update_parameters()
            return (self.opo.get_wavelength_nm(), self.opo.get_power(),
                    self.reference_set_point, self.reference_diode.get_voltage(),
                    self.motor.get_position())
        
    def calibrate_hwp_zero(self):
        current = self.reference_diode.get_voltage()
        last = current
        step = 5
        
        while (abs(step) > 0.001):
            self.motor.set_position(step, relative=True)
            time.sleep(0.5)
            
            last = current
            current = self.reference_diode.get_voltage()
            print("Diode:", current)
            
            if current < last:
                continue
            else:
                step *= -0.5
        
        print(self.motor.get_position())
        
    def calibrate_hwp_zero2(self):        
        def loop(step):
            while (abs(step) > 0.001):
                self.motor.set_position(step, relative=True)
                time.sleep(0.5)
                
                current = self.reference_diode.get_voltage()
                print("Diode:", current)
                if current != 0.0:
                    self.motor.set_position(-step, relative=True)
                    step *= 0.5
            print(self.motor.get_position())
            
        pos = self.motor.get_position()
        loop(1)
        self.motor.set_position(pos)
        loop(-1)
                    
    def start_power_adjustment_loop(self):
        if not self.reference_diode:
            return
        if self.power_adjustment_loop_run == False:
            self.power_adjustment_loop_run = True
            
            self.opo_update_loop_thread = \
                threading.Thread(target=self.__opo_update_loop)
            self.opo_update_loop_thread.start()

            self.power_adjustment_loop_thread = \
                threading.Thread(target=self.__power_adjustment_loop)
            self.power_adjustment_loop_thread.start()
        
    def stop_power_adjustment_loop(self):
        if self.power_adjustment_loop_run == True:
            self.power_adjustment_loop_run = False
            self.power_adjustment_loop_thread.join()
            self.opo_update_loop_thread.join()

    def __opo_update_loop(self):
        """
        A function that is started in a thread by start_power_adjustment_loop.
        Controls the OPO.
        """
        logging.info("Opo update loop started.")
        
        self.wl_adjust_time = -1
        
        while self.power_adjustment_loop_run == True:
#            print(time.perf_counter() - start_time)

            if self.close_opo_shutter:
                self.opo.set_shutter(False)
                self.close_opo_shutter = False
                self.open_opo_shutter = False
            elif self.open_opo_shutter:
                self.opo.set_shutter(True)
                self.close_opo_shutter = False
                self.open_opo_shutter = False
                
            if  self.adjust_wavelength == True:
                self.adjust_wavelength = False
                self.wl_adjust_time = 0
                self.opo.set_wavelength_nm(self.current_set_wavelength)
                continue
            
            self.opo.update_parameters()
            
            if self.opo.check_status_ok():
                self.wl_adjust_time = -1
            else:
                self.wl_adjust_time += 1
                if self.wl_adjust_time > 1200:
                    self.opo.repair()
                    self.wl_adjust_time = -1
                    
            try:                 
                self.reference_set_point = self.power_calibration.wl_power_get_voltage(
                    self.opo.get_wavelength_nm(), self.target_power)
            except Exception:
                logging.warning("Requested power: " + str(self.target_power) +
                                "could not be set.")
                              
        self.power_adjustment_loop_run = False
        logging.info("Opo update loop ended.")
        
    def __power_adjustment_loop(self):
        """
        A function that is started in a thread by start_power_adjustment_loop.
        Controls the HWP angle.
        """
        
        logging.info("Power adjustment loop started.")
        self.__set_power_stable(False)
        previous_error  = 0.0
        integral        = 0.0
        new_position    = self.motor.get_position()
        start_time      = time.perf_counter()
        stable          = [False] * 5
        self.current_reference_point = 0.0
        
#        test_m = Measurement() #"test.m", load_file=False)
#        test_m.set_labels_list(["Time (s)", "Waveplate Angle (degree)"])
#        test_m.start()
        
        while self.power_adjustment_loop_run == True:
            start_time = time.perf_counter()
            if  self.wl_adjust_time >= 0:
                self.__set_power_stable(False)
                stable = [False] * 5
            if self.opo.get_shutter() == False or self.opo.get_wavelength_read_successful() == False:
                self.__set_power_stable(False)
                stable = [False] * 5
                previous_error = 0.0
                integral = 0.0
                continue
                
            
            self.current_reference_point = self.reference_diode.get_voltage()
            
            end_time   = time.perf_counter()
            dt         = end_time - start_time
            start_time = end_time
            error          = self.reference_set_point - self.current_reference_point
            integral      += error * dt
            derivative     = (error - previous_error) / dt
            previous_error = error
                    
            self.current_read_angle = self.motor.get_position()
            
            new_position -= error * self.pid_p + integral * self.pid_i + derivative * self.pid_d
            if new_position < self.max_power_angle_at_wl(self.opo.get_wavelength_nm()):
                new_position = self.max_power_angle_at_wl(self.opo.get_wavelength_nm())
            elif new_position > self.min_power_angle_at_wl(self.opo.get_wavelength_nm()):
                new_position = self.min_power_angle_at_wl(self.opo.get_wavelength_nm())
              
            self.motor.correct_errors()
            self.motor.set_position(new_position, print_position=False)#, wait=False)
#            test_m.add_point([time.perf_counter(), new_position])
            
            try:
                if abs(error / self.reference_set_point) < self.max_relative_error:
                    stable.append(True)
                else:
                    stable.append(False)
                stable.pop(0)
            except Exception:
                logging.warning("Power control loop division by zero")
            
            self.__set_power_stable(all(stable))
            
        self.power_adjustment_loop_run = False
        logging.info("Power adjustment loop ended.")
        
#        test_m.end()
#        test_m.plot(y1max=-45, save=False)
  
    def __set_power_stable(self, stable):
        if stable:
            if (not self.power_stable):
                self.power_stable = True
                logging.info("Power stable: " + \
                             str(self.opo.get_wavelength_nm()) + "nm, " + \
                             str(self.target_power) + "W")
        else:
            if (self.power_stable):
                self.power_stable = False
                logging.info("Power unstable: " + \
                             str(self.opo.get_wavelength_nm()) + "nm, " + \
                             str(self.target_power) + "W")
