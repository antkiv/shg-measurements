# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import numpy as np
import matplotlib.pyplot as plt

class Sample:
    def __init__(self, name):
        self.__name = name
        self.__data = {}

    def addData(self, data, polarization, diffraction_angle):
        if polarization not in self.__data:
            self.__data[polarization] = {}

        self.__data[polarization][diffraction_angle] = data

    def plot(self):
        for polarization in self.__data:
            fig = plt.figure()
            ax1 = fig.add_subplot(111)
            ax1.set_title(self.__name + " " + polarization)
            ax1.set_xlabel('Sample angle (Degrees)')
            ax1.set_ylabel('Detector counts')

            for diffraction_angle in sorted(self.__data[polarization]):
                data = self.__data[polarization][diffraction_angle]

                x = data[:,0]
                y = data[:,1]
                plot = ax1.plot(x,y, label=str(diffraction_angle))
                plt.setp(plot, linewidth=0.7)

            ax1.set_ylim(ymin=0)
            ax1.legend(loc='best')
            
            file_name = self.__name + "_" + polarization + '.png'
            plt.savefig(file_name, dpi=600)
            print(file_name + " plotted.")
            plt.close()
            
def read_data_file(file, samples):
        data = np.loadtxt(file)
        
        #L1_yyy_70mW_0detect
        ns = file.split('_')
        name = ns[0]
        polarization = ns[1]
        diffraction_angle = ns[3].split('d')[0]

        if name not in samples:
            samples[name] = Sample(name)
        samples[name].addData(data, polarization, diffraction_angle)

def read_data_files(directory, samples):
    for root, root_dirs, data_files in os.walk(directory):
        break
        
    for file in data_files:
        if file.endswith('.txt'):
            print(file)
            read_data_file(file, samples)

def plot_all():
    samples = {}
    read_data_files('.', samples)
    
    for s in samples:
        samples[s].plot()
        
    print("Done")
