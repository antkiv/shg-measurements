# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from scipy import constants
import numpy as np

class WavelengthRange:
    """
    A class to handle a range of wavelengths.
    Can convert between wavelengths, wavenumbers and frequencies.
    """
    def __init__(self, wavelength_range_nm = []):
        self.set_wavelength_range_nm(wavelength_range_nm)
        
    def set_wavelength_range(self, wavelength_range):
        self.wl_range = np.array( wavelength_range )
        self.v_range  = 1 / self.wl_range
        self.f_range  = constants.c * self.v_range
        
    def set_wavelength_range_nm(self, wavelength_range):
        self.set_wavelength_range( np.array(wavelength_range) * 10**-9 )
        
    def set_frequency_range(self, frequency_range):
        self.f_range = np.array( frequency_range )
        self.wl_range = constants.c / self.f_range
        self.v_range  = self.f_range / constants.c
    
    def set_wavenumber_range(self, wavenumber_range):
        self.v_range = np.array(wavenumber_range)
        self.f_range  = constants.c * self.v_range
        self.wl_range = 1 / self.v_range
        
    def set_linear_wavenumber_range_from_wavelengths_nm(self, start, stop, steps):
        start *= 10**-9
        stop *= 10**-9        
        v_start = 1 / start
        v_stop = 1 / stop
        self.set_wavenumber_range(np.linspace(v_start, v_stop, steps))
        
    def wavelengths_m(self):
        return self.wl_range
    def wavelengths_nm(self):
        return self.wl_range * 10**9
        
    def wavenumbers_m(self):
        return self.v_range
    def wavenumbers_cm(self):
        return self.v_range * 10**-2
        
    def frequencies(self):
        return self.f_range
        