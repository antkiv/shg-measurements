# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Functions to help plot measurement results etc.
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
import os

import photonics as ph
from .wavelength_range import WavelengthRange
from .measurement import Measurement

time_str = "Time (s)"
wl_str = "Wavelength (nm)"
det_str = "Detector count"
ref_str = "Reference Detector count"
pow_str =  "Power (mW)"
diode_set_point_str = "Reference diode set point voltage (V)"
diode_str = "Reference diode voltage (V)"
angle_str = "HWP angle (degrees)"
opo_p_str = "Opo power (W)"
calib_power_str = "Calibration power meter power (W)"
wn_str = "Wavenumber (1/cm)"
det_power_str = "Detector power (W)"

dark_counts = 540.0

def wnr(steps, start=1002, stop=1298):
    wn = WavelengthRange()
    wn.set_linear_wavenumber_range_from_wavelengths_nm(start, stop, steps)
    return wn
def wlr(steps, start=1002, stop=1298):
    wl = WavelengthRange()
    wl.set_wavelength_range_nm(np.linspace(start,stop,steps))
    return wl        
def wavelength_to_sh_wavelength(x):
    V = x / 2
    return ["%.0f" % z for z in V]    
def wavenumber_cm_to_wavelength_nm(x):
    V = 1 / x * 10**7
    return ["%.0f" % z for z in V]    
def switch_wavelenght_frequency(x):
    V = scipy.constants.c / x
    return ["%.0f" % z for z in V]
        
def plot_dir_sh_wl(directory):
    plot_from_dir(directory, ".wl", Measurement.plot,
                  (0,1,2, wavelength_to_sh_wavelength, "SHG wavelength (nm)"),
                  show=True)
        
def plot_freq_scale(m):
    x1 = m.get_label_index(wl_str)
    y1 = m.get_label_index(det_str)
    y2 = m.get_label_index(ref_str)
    w = WavelengthRange(m.get_column(x1))
    
    m.add_column(w.frequencies())
    
    xcolumn = m.column_count() - 1
    
    print(xcolumn)
    m.sort_by_column(xcolumn)
    m.plot(xcolumn, x1, y1, y2)
def plot_wavenumber_wl(m):
    w = WavelengthRange(m.get_label_data(wl_str))
    m.add_column(w.wavenumbers_cm(), wn_str)
    m.sort_by_column(m.get_label_index(wn_str), inverse=True)
    
    wl_min = int( min(w.wavelengths_nm()) )
    wl_max = int( max(w.wavelengths_nm() + 0.5) )
    wl_range = WavelengthRange(np.linspace(wl_min, wl_max, 10))
    
    m.plot(m.get_label_index(wn_str),
           m.get_label_index(det_str),
           m.get_label_index(ref_str),
           "Wavelength (nm)\n SHG Wavelength (nm)",
           [str(int(i)) + '\n' + str(int(i/2)) for i in wl_range.wavelengths_nm()],
            wl_range.wavenumbers_cm())   
    

def plot_file(file):
    try:
        if file.endswith(".wl"):
            m = Measurement(file)
#            m.add_measured_power_column_from_counts()
            x1 = m.get_label_index(wl_str)
#            y1 = m.get_label_index(det_power_str)
            y1 = m.get_label_index(det_str)
            y2 = m.get_label_index(ref_str)
            
            m.plot(x1column = x1, y1column = y1, y2column = y2,
                  x2_label = None, x2_ticks = None, x2_tick_pos = None,
                  y1max = None, y2max = None, limits = True,
                  show = False, save = True)
                  
        elif file.endswith(".wla"):
            pass
            
        elif file.endswith(".power"):
            m = Measurement(file)
            x1 = m.get_label_index(pow_str)
            y1 = m.get_label_index(det_str)
            y2 = m.get_label_index(ref_str)
            
            m.plot(x1column = x1, y1column = y1, y2column = y2,
                  x2_label = None, x2_ticks = None, x2_tick_pos = None,
                  y1max = None, y2max = None, limits = True,
                  show = False, save = True, line_width=2)
                  
        elif file.endswith(".rpc"):
            m = Measurement(file)
            x1 = m.get_label_index(wl_str)
            y1 = m.get_label_index(calib_power_str)
            y2 = m.get_label_index(ref_str)
            
            m.plot(x1column = x1, y1column = y1, y2column = y2,
                  x2_label = None, x2_ticks = None, x2_tick_pos = None,
                  y1max = None, y2max = None, limits = True,
                  show = False, save = True)
            
        elif file.endswith(".ipc"):
            m = Measurement(file)
            wl_index    = m.get_label_index(wl_str)
            diode_index = m.get_label_index(diode_str)
            pm_index    = m.get_label_index(calib_power_str)
            
            angle_index = m.get_label_index(angle_str)
            angle_data = {}
            
            for row in m.points:
                angle = row[angle_index]
                if angle in angle_data:
                    angle_data[angle].append(row)
                else:
                    angle_data[angle] = [row]
            
            fig, ax1, ax2 = m.start_plot(x1column = wl_index, y1column = diode_index, y2column = pm_index)
            i = -1
            for angle in sorted(angle_data, reverse=True):
                i += 1
                if i % int((len(angle_data) / 3.0) + 0.5) != 0:
                    continue
                data = np.array(angle_data[angle])
                ax1.plot(data[:, wl_index], data[:, diode_index], label = int(angle + 0.5))
                ax2.plot(data[:, wl_index], data[:, pm_index], "--")    
            ax1.set_ylim(ymin=0)
            ax2.set_ylim(ymin=0)
            ax1.legend()
            m.end_plot(fig, ax1, ax2, show = True, save = True)
            
        elif file.endswith(".opc"):
            m = Measurement(file)
            x1 = m.get_label_index(wl_str)
            pmt_index = m.get_label_index(det_str)
            spm_index = m.get_label_index(calib_power_str)
            
            angle_column_index = m.get_label_index(angle_str)
            pmt_data = {}
            spm_data = {}
            
            for row in m.points:
                trace = None
                if row[pmt_index] == 0.0 or row[pmt_index] == 0:
                    trace = spm_data
                elif row[spm_index] == 0.0 or row[spm_index] == 0:
                    trace = pmt_data
                else:
                    print("No data")
                    continue
                    
                angle = row[angle_column_index]
                if angle in trace:
                    trace[angle].append(row)
                else:
                    trace[angle] = [row]
            
            fig, ax1, ax2 = m.start_plot(x1column = x1, y1column = pmt_index, y2column = spm_index)
            
            for angle in pmt_data:
                data = np.array(pmt_data[angle])
                pmt_data_t = data[:, pmt_index] - dark_counts
                ax1.plot(data[:, x1], pmt_data_t)
                
            for angle in spm_data:
                data = np.array(spm_data[angle])
                spm_data_t = data[:, spm_index]# * 1e12
                ax2.plot(data[:, x1], spm_data_t, "--")
                
            ax1.set_ylim(ymin=0)
            ax2.set_ylim(ymin=0)
            m.end_plot(fig, ax1, ax2, show = False, save = True)
                
        elif file.endswith(".time"):
            m = Measurement(file)
            x1 = m.get_label_index(time_str)
            y1 = m.get_label_index(det_str)
            y2 = m.get_label_index(ref_str)
            
            m.plot(x1column = x1, y1column = y1, y2column = y2,
                  x2_label = None, x2_ticks = None, x2_tick_pos = None,
                  y1max = None, y2max = None, limits = True,
                  show = False, save = True)

        else:
            pass
    except:
        print("Could not plot: " + file)
        plt.close()
    
def plot_all_recursively(directory, replace = False):
    for root, dirs, files in os.walk(directory):
        for f in files:
            filename = root + '/' + f
            if os.path.isfile(filename + ".pdf") == False or replace == True:
                plot_file(root + '/' + f)
        for d in dirs:
            plot_all_recursively(root + d)
        