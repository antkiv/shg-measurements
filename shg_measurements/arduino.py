# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import serial
import time
import logging
import threading

from photonics.devices.power_meter import PowerMeter

class Arduino():
    """
    An Arduino microcontroller board is used to interface a photodiode and
    control mechanical shutter and mirror movements.
    """
    ADC_READ = 'a';
    BUFFER_AVG_READ = 'b';
    SHUTTER_CLOSE = 'c';
    MIRROR_DOWN = 'd';
    MIRROR_CORRECT_POS = 'e';
    MIRROR_INCORRECT_POS = 'i';
    MIRROR_MOVING = 'm';
    SHUTTER_OPEN = 'o';
    MIRROR_RESET = 'r';
    MIRROR_STATE = 's';
    MIRROR_UP = 'u';

    def __init__(self, serial_port, power_calibration):
        self.ser = serial.Serial(serial_port, baudrate=115200, timeout=1,
                                 parity=serial.PARITY_NONE, bytesize=8, stopbits=1)
        logging.debug("Arduino serial: " + self.ser.name)
        self.ser.read_all()
        
        # Mutex is used whenever serial communication is done.
        self.mutex = threading.Lock()
        
        self.reference_diode = ArduinoReferenceDiode(self, power_calibration)
        
#        # Initialize the mirror by moving it a few times
#        self.mirror_up()
#        self.reset_mirror()
#        self.mirror_up()
#        self.reset_mirror()
#        self.mirror_down()
#        self.reset_mirror()
        self.mirror_at_pmt = True
        
    def close(self):
        with self.mutex:
            self.ser.close()
        
    def __read(self):
        """
        Read serial port omitting newline.
        """
        return self.ser.readline().decode()[0:-2]
            
    def get_reference_diode(self):
        return self.reference_diode
    
    def get_diode_voltage(self):
        """
        Returns the raw photodiode voltage in Volts.
        """
        with self.mutex:
            self.ser.read_all()        
            self.ser.reset_input_buffer()
            while True:
                ret = 0
                try:
                    self.ser.write(self.BUFFER_AVG_READ.encode('ascii'))
                    meas = self.__read()
                    # 3.3 * (0.326 + 0.669) / (0.326 * 4095.0) = voltage step in theory
                    # 3.3 Volts, 0.669 kOhm and 0.326 kOhm resistor divider.
                    # 12bit adc. -> max value 4095
                    # measured voltage and 12bit adc + 100 averages.
                    # 10.590 is an experimental value
                    ret = float(meas) * 10.590 / (4095 * 100)
                except:
                    logging.debug("Arduino diode reading failed.")
                    continue
                return ret
    
    def set_pmt_shutter_open(self, state=False):
        """
        state == False closes the shutter and True opens it.
        """
        with self.mutex:
            if state == True:
                self.ser.write(self.SHUTTER_OPEN.encode('ascii'))
                logging.info("PMT shutter opened.")
            else:
                self.ser.write(self.SHUTTER_CLOSE.encode('ascii'))
                logging.info("PMT shutter closed.")
        
    def mirror_move(self, up=True):
        """
        Old mirror movement code. Not in use.
        """
        direction = self.MIRROR_DOWN
        if up:
            direction = self.MIRROR_UP
            
        trial = 0
        while True :
            with self.mutex:
                self.ser.read_all()
                self.ser.write(direction.encode('ascii'))
                read = self.__read()
            
            if read == direction:
                break
            elif read == self.MIRROR_MOVING:
                if trial > 3:
                    logging.error("Mirror still moving!")
                    break
                logging.warning("Mirror moving, waiting...")
                time.sleep(1)
                trial += 1
            else:
                logging.error("Mirror jammed!")
                break
            
    def mirror_up(self):
        """
        Old mirror movement code. Not in use.
        """
        self.mirror_move(up=True)
    def mirror_down(self):
        """
        Old mirror movement code. Not in use.
        """
        self.mirror_move(up=False)
    def reset_mirror(self):
        """
        Old mirror movement code. Not in use.
        """
        with self.mutex:
            self.ser.write(self.MIRROR_RESET.encode('ascii'))
            self.ser.read_all()
        self.mirror_at_pmt = True

    def rotate_mirror_pmt(self, state=True):
        """
        Old mirror movement code. Not in use.
        """
        if state == True:
            if self.mirror_at_pmt == False:
                self.mirror_down()
                self.mirror_at_pmt = True
                logging.info("Mirror set to PMT.")
            else:
                logging.info("Mirror already at PMT.")
        else:
            if self.mirror_at_pmt == True:
                self.mirror_up()
                self.mirror_at_pmt = False
                logging.info("Mirror set to power meter.")
            else:
                logging.info("Mirror already at power meter.")


        
class ArduinoReferenceDiode(PowerMeter):
    """
    Arduino photodiode voltage readings can be calibrated and used as
    a power meter with this class.
    """
    
    def __init__(self, arduino, power_calibration):
        self.arduino = arduino
        self.power_calibration = power_calibration

    def get_voltage(self):
        """
        Returns the raw photodiode voltage in Volts.
        """
        return self.arduino.get_diode_voltage()
        
    def get_power(self, wavelength_nm = 0):
        """
        Returns photodiode power using a power calibration table.
        Only accurate if correct wavelength is provided. If wavelenght is 0 (default)
        Then the wavelenght is set by set_wavelength_nm() from parent class PowerMeter.
        """
        if wavelength_nm == 0:
            wavelength_nm = self.wavelength_nanometers
        return self.power_calibration.wl_voltage_get_power(wavelength_nm, self.get_voltage())
                
    def get_hwp_angle(self, wavelength_nm = 0):
        """
        Returns HWP angle approximation using a power calibration table.
        Only accurate if correct wavelength is provided. If wavelenght is 0 (default)
        Then the wavelenght is set by set_wavelength_nm() from parent class PowerMeter.
        """
        if wavelength_nm == 0:
            wavelength_nm = self.wavelength_nanometers
        return self.power_calibration.wl_voltage_get_hwp_angle(wavelength_nm, self.get_voltage())
                
    def get_opo_power(self, wavelength_nm = 0):
        """
        Returns maximum OPO power approximation using a power calibration table.
        Only accurate if correct wavelength is provided. If wavelenght is 0 (default)
        Then the wavelenght is set by set_wavelength_nm() from parent class PowerMeter.
        """
        if wavelength_nm == 0:
            wavelength_nm = self.wavelength_nanometers
        return self.power_calibration.wl_voltage_get_opo_power(wavelength_nm, self.get_voltage())
    