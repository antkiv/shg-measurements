# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import atexit
import time
import logging
import os

import photonics as ph

from photonics.devices.photon_counter import PhotonCounter
from photonics.devices.photon_counter_becker_hickl import PhotonCounterBeckerHickl
from photonics.devices.opo_chameleon_compact import OpoChameleonCompact
from photonics.devices.motor import MotorController, Motor
from photonics.devices.motor_newport import MotorControllerNewport, MotorNewport
from photonics.devices.power_meter import PowerMeter
from photonics.devices.power_meter_ophir import PowerMeterOphir
from photonics.devices.power_meter_thorlabs import PowerMeterThorlabs
from .arduino import Arduino, ArduinoReferenceDiode
from .power_calibration import PowerCalibration
from .power_control import PowerControl
from .measurement import Measurement
    
class ShgMeasurements:
    """
    Main class of the program. There should be only one object of this class
    and it is created by init() in main_shg_measurements.py. Termination is handled
    by term() in the same file. The object is called sm.
    """
    
    def __init__(self):
        self.photon_counter = None
        self.motor_controller = None
        self.motor = []
        self.power_control_motor = None
        self.polarization_control_motor = None
        self.arduino = None
        self.reference_diode = None
        self.opo = None
        self.power_meter_ophir = None
        self.power_meter_thorlabs = None
        self.measurement_power_meter = None
        self.power_control = None
        self.power_calibration = None
        self.last_measurement = None
        
    
    def initialize(self,
                   opo_port = "/dev/ttyUSB1",
                   motor_controller_port = "/dev/ttyUSB0",
                   arduino_port = "/dev/ttyACM0",
                   power_meter_ophir_port = "/dev/ttyUSB2",
                   power_meter_thorlabs = True,
                   photon_counter_port = "/dev/pts/0",
                   power_calibration_file = "../power_calibrations/file.ipc"):
        #Do not modify the default parameters here for init() anymore. Modify in init().

        ##########
        #Setup logging
        self.running_directory = os.getcwd()
        date = time.strftime("%Y%m%d")
        os.chdir("shg_results")
        if not os.path.isdir(date):
            os.mkdir(date)
        os.chdir(date)
        
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        
        # Logging into console
#        self.console_log_handler = logging.StreamHandler()
#        self.console_log_handler.setLevel(logging.INFO)
#        chf = logging.Formatter('%(message)s')
#        self.console_log_handler.setFormatter(chf)
        
        self.file_log_handler = logging.FileHandler("shg_measurements.log")
        self.file_log_handler.setLevel(logging.DEBUG)
        fhf = logging.Formatter('%(asctime)s %(levelname)8s: %(message)s')
        self.file_log_handler.setFormatter(fhf)
        
        logger.addHandler(self.file_log_handler)
        # Logging into console
#        logger.addHandler(self.console_log_handler)
        ##########
        
        logging.info("-----Photon Counter-----")
        if photon_counter_port:
            self.photon_counter = PhotonCounterBeckerHickl(photon_counter_port)
        else:
            self.photon_counter = PhotonCounter()
        
        logging.info("-----OPO-----")
        if opo_port != None:
            self.opo = OpoChameleonCompact(opo_port)
        
        logging.info("-----Motor controller-----")
        if motor_controller_port != None:
            self.motor_controller = MotorControllerNewport(serial_port=motor_controller_port, baud_rate=19200)
            self.motor_controller.append_motor(1)
            self.motor_controller.append_motor(2)
            self.motor = self.motor_controller.motor
            self.power_control_motor = self.motor[1]
            self.polarization_control_motor = self.motor[0]
            
        if power_calibration_file != None:
            self.power_calibration = PowerCalibration(power_calibration_file)
            
        if arduino_port != None:
            self.arduino = Arduino(arduino_port, self.power_calibration)
            self.reference_diode = self.arduino.get_reference_diode()
    
        if power_meter_ophir_port != None:
            self.power_meter_ophir = PowerMeterOphir(power_meter_ophir_port)
    
        if power_meter_thorlabs == True:
            self.power_meter_thorlabs = PowerMeterThorlabs()
    
        if self.opo != None:
            logging.info("-----OPO-----")
            self.opo.print_parameters(logging.info)
            
        self.measurement_power_meter = PowerMeter()

        if self.power_control_motor and self.opo and self.arduino:
            self.power_control = PowerControl(self.power_control_motor,
                                              self.opo,
                                              self.reference_diode,
                                              self.power_calibration)
            self.power_calibration.init_calibration_devices(self.opo,
                                                            self.power_control_motor,
                                                            self.power_meter_ophir,
                                                            self.arduino,
                                                            self.reference_diode,
                                                            self.photon_counter)
        else:
            self.power_control = PowerControl(None, None, None, None)
            
        atexit.register(ShgMeasurements.terminate, self)
        
    def terminate(self):
        logging.info("Closing...")
        
        if self.power_control:
            self.power_control.stop_power_adjustment_loop()
    
        if self.opo != None:
            self.opo.set_shutter(False, check=True)
            self.opo.close()
            self.opo = None
            logging.info("Opo")
            
        if self.power_meter_ophir != None:
            self.power_meter_ophir.close()
            self.power_meter_ophir = None
            logging.info("Ophir power meter")
            
        if self.power_meter_thorlabs != None:
            self.power_meter_thorlabs.close()
            self.power_meter_thorlabs = None
            logging.info("Thorlabs power meter")
            
        if self.arduino != None:
            self.arduino.close()
            self.arduino = None
            logging.info("Arduino")
            
        if self.motor_controller != None:
            self.motor[0].set_position(0, wait=False)
            self.motor[1].set_position(0, wait=False)
            self.motor_controller.close()
            self.motor_controller = None
            logging.info("Motor controller")
            
        logger = logging.getLogger()        
#        logger.removeHandler(self.console_log_handler)
        logger.removeHandler(self.file_log_handler)
        os.chdir(self.running_directory)
        
    def get_running_directory(self):
        return self.running_directory
        
    def measure(self, measure_pmt, measure_diode, measure_power):
        if self.photon_counter != None and measure_pmt:
            print("Measured:", self.photon_counter.measure(), "counts.")
        if self.reference_diode != None and measure_diode:
            print("Diode: ", self.reference_diode.get_voltage())
        if measure_power == True:
            if self.power_meter_ophir != None:
                print("Ophir power: ",
                      self.power_meter_ophir.get_power(wait=False))
            if self.power_meter_thorlabs != None:
                print("Thorlabs power: ",
                      self.power_meter_thorlabs.get_power(wait=False))

                        
    def do_measurement(self, measurement, generator, x1, y1, y2, plot=True,
                       realtime=False, test=False, power_loop=True):
        self.last_measurement = measurement
        
        measurement.set_labels_list(["Time (s)",
                                "Wavelength (nm)",
                                "Detector count",
                                "Reference Detector count",
                                "Power (mW)",
                                "Reference diode set point voltage (V)",
                                "Reference diode voltage (V)",
                                "HWP angle (degrees)",
                                "Opo power (W)",
                                "Polarization control HWP angle (degrees)"])
        if not test:
            measurement.set_comment(
                "Wavelength: " + str(self.opo.get_target_wavelength_nm()) +
                " nm , Power: " + str(self.power_control.get_target_power_mw()) +
                " mW , PMS measure time: " + str(self.photon_counter.get_measurement_time()) +
                " s , Averages: " + str(self.photon_counter.get_averaging()) )
            if power_loop:
                self.power_control.stop_power_adjustment_loop()
                self.power_control_motor.set_position(0)
                self.power_control.set_opo_shutter(True)
                self.arduino.set_pmt_shutter_open(True)
                self.power_control.start_power_adjustment_loop()
                
        measurement.start()
        if realtime:
            measurement.animate(generator(), x1, y1, y2)
        else:
            for i in generator():
                if not ph.stopper.is_running():
                    break                       
        measurement.end()
        
        if test:
            return
        measurement.save()      
        if power_loop:
            self.power_control.stop_power_adjustment_loop()
            self.arduino.set_pmt_shutter_open(False)
            self.power_control.set_opo_shutter(False)
            self.power_control_motor.set_position(0, wait=False)
        measurement.plot(x1, y1, y2)
        
    def measure_test(self, wavelength_range, repeat_times = 1,
                     plot=True, realtime = False):
        measurement = Measurement()
        
        def generator():
            for current_wl in wavelength_range.wavelengths_nm():
                for repeat in range(repeat_times):                             
                    measurement.add_point([measurement.get_time(),
                                           current_wl,
                                           repeat,
                                           0,0,0,0,0,0],
                                           True)
                    yield
                    time.sleep(0.05)
                    
        args = (self, measurement, generator, 1, 2, 3, plot, realtime, True)
        ph.stopper.start_function(ShgMeasurements.do_measurement, args)
        
        if plot:
            measurement.plot(0,1,2)
            
        return measurement
        
                        
    def measure_wavelengths(self, filename, wavelength_range, repeat_times = 1,
                            plot=True, realtime = False, stopper=True):  
        measurement = Measurement(filename, load_file=False)
        
        def generator():
            for current_wl in wavelength_range.wavelengths_nm():
                self.power_control.set_wavelength_nm(current_wl, wait=True)
                
                for repeat in range(repeat_times):
                    self.power_control.wait_power_stable()
                    counts = self.photon_counter.measure()
                    
                    (current_opo_wavelength, current_opo_power,
                     reference_set_point, current_reference_point,
                     current_read_angle) = self.power_control.get_loop_parameters()
                             
                    measurement.add_point([measurement.get_time(),
                                           current_opo_wavelength,
                                           counts[1], counts[0],
                                           self.measurement_power_meter.get_power(wait=False),
                                           reference_set_point, current_reference_point,
                                           current_read_angle, current_opo_power,
                                           self.polarization_control_motor.get_position()],
                                           True)
                    yield
                    
        args = (self, measurement, generator, 1, 2, 3, plot, realtime)
        ph.stopper.start_function(ShgMeasurements.do_measurement, args)
            
        return measurement
    
    def measure_wavelengths_hwp_angle(self, filename, wavelength_range, hwp_angles,
                                      repeat_times = 1, plot = True, reverse = False,
                                      realtime = False):  
        measurement = Measurement(filename, load_file=False)
        if reverse:                    
            hwp_angles.reverse()
        
        def generator():
            for current_wl in wavelength_range.wavelengths_nm():
                self.power_control.set_wavelength_nm(current_wl, wait=True)
                if reverse:                    
                    hwp_angles.reverse()
                
                for current_hwp_angle in hwp_angles:
                    self.polarization_control_motor.set_position(current_hwp_angle)
                    
                    for repeat in range(repeat_times):
                        self.power_control.wait_power_stable()
                        counts = self.photon_counter.measure()
                        
                        (current_opo_wavelength, current_opo_power,
                         reference_set_point, current_reference_point,
                         current_read_angle) = self.power_control.get_loop_parameters()
                                 
                        measurement.add_point([measurement.get_time(),
                                               current_opo_wavelength,
                                               counts[1], counts[0],
                                               self.measurement_power_meter.get_power(wait=False),
                                               reference_set_point, current_reference_point,
                                               current_read_angle, current_opo_power,
                                               self.polarization_control_motor.get_position()],
                                               True)
                        yield
                        
        self.do_measurement(measurement, generator, 0, 1, 2, realtime=realtime)       
        
        
        args = (self, measurement, generator, 0, 1, 2, plot, realtime)
        ph.stopper.start_function(ShgMeasurements.do_measurement, args)
            
        return measurement
        
    def measure_hwp_angles(self, filename, hwp_angles,
                           repeat_times = 1, plot = True, reverse = False,
                           realtime = False):  
        measurement = Measurement(filename, load_file=False)
        if reverse:                    
            hwp_angles.reverse()
        
        def generator():          
            for current_hwp_angle in hwp_angles:
                self.polarization_control_motor.set_position(current_hwp_angle)
                
                for repeat in range(repeat_times):
                    self.power_control.wait_power_stable()
                    counts = self.photon_counter.measure()
                    
                    (current_opo_wavelength, current_opo_power,
                     reference_set_point, current_reference_point,
                     current_read_angle) = self.power_control.get_loop_parameters()
                             
                    measurement.add_point([measurement.get_time(),
                                           current_opo_wavelength,
                                           counts[1], counts[0],
                                           self.measurement_power_meter.get_power(wait=False),
                                           reference_set_point, current_reference_point,
                                           current_read_angle, current_opo_power,
                                           self.polarization_control_motor.get_position()],
                                           True)
                    yield
                        
        self.do_measurement(measurement, generator, 9, 1, 2, realtime=realtime)       
        
        args = (self, measurement, generator, 0, 1, 2, plot, realtime)
        ph.stopper.start_function(ShgMeasurements.do_measurement, args)
            
        return measurement
        
    def measure_powers(self, filename, powers_mW, plot=True, realtime = False): 
        measurement = Measurement(filename, load_file=False)
        
        def generator():
            for current_power in powers_mW:
                self.power_control.set_target_power_mw(current_power)
                self.power_control.wait_power_stable()
                
                counts = self.photon_counter.measure()
                
                (current_opo_wavelength, current_opo_power,
                 reference_set_point, current_reference_point,
                 current_read_angle) = self.power_control.get_loop_parameters()
             
                measurement.add_point([measurement.get_time(),
                                       current_opo_wavelength,
                                       counts[1], counts[0],
                                       current_power,
                                       reference_set_point, current_reference_point,
                                       current_read_angle, current_opo_power,
                                       self.polarization_control_motor.get_position()],
                                       True)
                yield
        
        args = (self, measurement, generator, 4, 2, 3, plot, realtime)
        ph.stopper.start_function(ShgMeasurements.do_measurement, args)
        
        return measurement
                        
    def measure_in_time(self, filename, delay_s, plot=True, power_loop=True,
                        realtime = False):     
        measurement = Measurement(filename, load_file=False)
        
        def generator():
            while True:
                time.sleep(delay_s)
                
                if power_loop:
                    self.power_control.wait_power_stable()
                counts = self.photon_counter.measure()
                
                (current_opo_wavelength, current_opo_power,
                 reference_set_point, current_reference_point,
                 current_read_angle) = self.power_control.get_loop_parameters()
                         
                measurement.add_point([measurement.get_time(),
                                       current_opo_wavelength,
                                       counts[1], counts[0],
                                       self.measurement_power_meter.get_power(wait=False),
                                       reference_set_point, current_reference_point,
                                       current_read_angle, current_opo_power,
                                       self.polarization_control_motor.get_position()],
                                       True)
                yield
            
        args = (self, measurement, generator, 0, 2, 3, plot, realtime, False, power_loop)
        ph.stopper.start_function(ShgMeasurements.do_measurement, args)
        
        return measurement
                  
    def measure_opo_transition_time(self, wavelength_range):
        self.power_control.set_wavelength_nm(
            wavelength_range.wavelengths_nm()[0], wait=True)

        measurement = Measurement()
        measurement.start()        
        self.power_control.start_measuring()
        
        def measure():
            for current_wl in wavelength_range:
                if not ph.stopper.is_running():
                    break
                self.power_control.set_wavelength_nm(current_wl, wait=True)
            
        #stopper.start_function(measure)
        measure()
        measurement.end()
        self.power_control.stop_measuring()
        