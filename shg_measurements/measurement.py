# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import time
import numpy as np
import matplotlib.pyplot as plt
import logging
from matplotlib import animation
from scipy import interpolate

import photonics as ph

class Measurement:
    """
    Stores one measurement series in a csv file. Supports quick plotting of
    the data.
    """
    
    def __init__(self, filename=None, load_file=True):
        self.points = np.array([])
        self.labels = [""]
        self.comments = ""
        self.started = False
        
        if load_file:
            if filename:
                self.load(filename)
            else:
                self.filename = "noname"
        else:
            self.filename = filename
        
    def start(self):
        """
        Starts measurement and records start time. Measurement points can now
        be added with add_point.
        """
        self.started = True
        self.m_points = []
        self.start_time = time.perf_counter()
   
    def end(self):
        """
        Ends mesurement.
        """
        self.started = False
        # Convert list to np array and discard the list.
        self.points = np.array(self.m_points)
        self.m_points = None
        
        self.end_time = time.perf_counter();
        logging.info("Measurements took: " + 
                     str(self.end_time - self.start_time) + " seconds")
        
        # Play a sound to notify the measurement has ended.
        ph.helpers.playsound(1500, 500)
        ph.helpers.playsound(1200, 500)
        ph.helpers.playsound(1500, 1000)
        
    def get_time(self):
        """
        Returns measurement time from the start of the measurement.
        """
        return time.perf_counter() - self.start_time
        
    def add_point(self, point, print_point=False):
        """
        Adds a measurement point that is a 1D list. If there are less elements
        in the list than the label list, zeros are added.
        """
        
        while len(point) < len(self.labels):
            point.append(0.0)
            
        if print_point:
            print(point)
        self.m_points.append(point)

    def set_labels_list(self, column_labels):
        """
        Measurement point labels in the same order.
        """
        self.labels = column_labels
        
    def set_comment(self, comment):
        """
        Adds a comment to the file header.
        """
        self.comments = comment
    
    def add_column(self, column_data, label="New column label"):
        """
        Add a column to a finished measurement. Useful in adding values
        calculated from the measurement and then plotting it.
        """
        c_data = np.array(column_data)[np.newaxis].T
        self.points = np.append(self.points, c_data, axis=1)
        self.labels.append(label)
        
    def get_label_index(self, label_str):
        return self.labels.index(label_str)
        
    def get_label_data(self, label_str):
        return self.get_column( self.get_label_index(label_str) )
        
    def get_column(self, column):
        return self.points[:, column]
        
    def column_count(self):
        return self.points.shape[1]
        
    def row_count(self):
        return self.points.shape[0]
        
    def save(self, filename=None):
        """
        Saves a measurement, that has ended, into a csv file.
        """
        if filename != None:
            self.filename = filename
        
        file_name = ph.helpers.check_exists_fix_filename(self.filename)
        np.savetxt(file_name, self.points, delimiter=";",
                   header=";".join(self.labels) + ";!" + self.comments)
        
    def load(self, filename=None):
        """
        Loads a csv file.
        """
        if filename != None:
            self.filename = filename
            
        self.points = np.loadtxt(self.filename, delimiter=";")

        file = open(self.filename)

        header_split = file.readline()[2:-1].split(";!")
        self.labels = header_split[0].split(";")
        if len(header_split) > 1:
            self.comments = header_split[1]
        
        file.close()
    
    def append(self, filename=None):
        """
        Appends a measurement file at the end of current loaded measurement.
        """
        if filename != None:
            points = np.loadtxt(filename, delimiter=";")
            self.points = np.concatenate((self.points, points))
            
    def sort_by_column(self, column, inverse=False):
        if inverse:
            self.points = self.points[ self.points[:,column].argsort()[::-1] ]
        else:
            self.points = self.points[ self.points[:,column].argsort() ]
        
    def add_measured_power_column_from_counts(self, power_per_count=5.2e-18,
                                              transmission_file="total_transmission"):
        """
        Adds measured power (Detector power) column into the measurement. Takes into account
        the transmission file.
        """
        wavelength = self.get_label_data("Wavelength (nm)")
        counts = self.get_label_data("Detector count")
        total_transmission = np.loadtxt(transmission_file, delimiter=";")
        wl_to_transmission_interp = interpolate.interp1d(total_transmission[:,0], total_transmission[:,1])
        
        power = counts * power_per_count / wl_to_transmission_interp(wavelength * 0.5)
        self.add_column(power, "Detector power (W)")
        
    def plot_ax(self, ax1, x1column = 0, y1column = 1, ax2 = None,  y2column = None):
        x = self.get_column(x1column)
        y1 = self.get_column(y1column)
        line = ax1.plot(x, y1)

        if y2column != None:            
            y2 = self.get_column(y2column)
            line.append( ax2.plot(x, y2, 'g') )
                
        return line
            
            
    def animate(self, frames_generator, x1column = 0, y1column = 1, y2column = None):
        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.set_xlabel(self.labels[x1column])
        ax1.set_ylabel(self.labels[y1column], color="b")
            
        x1 = []
        y1 = []
        y2 = []
        x1lim = [np.inf, -np.inf]
        y1lim = [0, -np.inf]
        y2lim = [0, -np.inf]
        lines1, = ax1.plot([], [], color="b")
        lines2 = None
        
        if y2column != None:
            ax2 = ax1.twinx()
            ax2.set_ylabel(self.labels[y2column], color="g")
            lines2, = ax2.plot([], [], color="g")
        else:
            ax1.grid()
            
        def func(args):
            nx1 = self.m_points[-1][x1column]
            ny1 = self.m_points[-1][y1column]
            
            x1.append(nx1)
            y1.append(ny1)
            x1lim[0] = min(x1lim[0], nx1)
            x1lim[1] = max(x1lim[1], nx1)
            y1lim[0] = min(y1lim[0], ny1)
            y1lim[1] = max(y1lim[1], ny1)
            ax1.set_xlim(x1lim)
            ax1.set_ylim(y1lim)
            #ax1.figure.canvas.draw()
            lines1.set_data(x1, y1)
            
            if y2column:
                ny2 = self.m_points[-1][y2column]
                y2.append(ny2)
                y2lim[0] = min(y2lim[0], ny2)
                y2lim[1] = max(y2lim[1], ny2)
                ax2.set_ylim(y2lim)
                lines2.set_data(x1, y2)
                
            return lines1,
            
        anim = animation.FuncAnimation(fig, func, frames=frames_generator,
                                       repeat=False, interval = 0)
        
        plt.show()
        while plt.get_fignums():
            plt.pause(0.5)
            
    def start_plot(self, x1column = 0, y1column = 1, y2column = None, title=True):
        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        if title:
            ax1.set_title(self.filename + "\n" + self.comments, y = 1.22)
        ax1.set_xlabel(self.labels[x1column])
        ax1.set_ylabel(self.labels[y1column], color="b")
        
        ax2 = None
        if y2column != None:
            ax2 = ax1.twinx()
            ax2.set_ylabel(self.labels[y2column], color="g")
        
        return fig, ax1, ax2
    def plot(self, x1column = 0, y1column = 1, y2column = None,
             x2_label = None, x2_ticks = None, x2_tick_pos = None,
             y1max = None, y2max = None, limits = True,
             show = True, save = True, title=True, line_width = 0.7):
        fig, ax1, ax2 = self.start_plot(x1column, y1column, y2column, title)
            
        line = self.plot_ax(ax1, x1column, y1column, ax2, y2column)
        plt.setp(line, linewidth=line_width)
        
        x = self.get_column(x1column)
        ax1.set_xlim(xmin=x[0], xmax=x[-1])
        
        if limits:
            if y1max:
                ax1.set_ylim(ymin=0, ymax=y1max)
            else:
                ax1.set_ylim(ymin=0)  
            if ax2:
                if y2max:
                    ax2.set_ylim(ymin=0, ymax=y2max)
                else:
                    ax2.set_ylim(ymin=0)
        
        if x2_label:
            ax3 = ax1.twiny()
            ax3.set_xlabel(x2_label)
            ax3.set_xticks(x2_tick_pos)
            ax3.set_xticklabels(x2_ticks)
            ax3.set_xlim(ax1.get_xlim())

        self.end_plot(fig, ax1, ax2, show, save)
    def end_plot(self, fig, ax1, ax2, show=True, save=True):
        fig.set_tight_layout(True)
            
        if save == True:
            file_name = self.filename + '.pdf'
            #file_name = check_exists_fix_filename(file_name)
            fig.savefig(file_name)
            print(file_name + " plotted.")
        if show == True:
            fig.show()
        else:
            plt.close(fig)
        
def combine_measurements_from_dir(postfix, src1, src2, dest, sort_column = 0):
    for root, root_dirs, data_files in os.walk(src1):
        break
        
    for file in data_files:
        if file.endswith(postfix):
            m = Measurement()
            m.load(src1 + "/" + file)
            m.append(src2 + "/" + file)
            m.sort_by_column(sort_column)
            m.save(dest + "/" + file)