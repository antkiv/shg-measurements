/*
 Copyright 2019 Antti Kiviniemi
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <Servo.h>

const unsigned int BUFSIZE = 100;
unsigned int adcRingBuffer[BUFSIZE] = {0};
unsigned int ringPos = 0;

Servo shutter_servo;

const int shutterServoPin = 9;
const int adcReadPin = A1;
const int shutterPin =  13;
const int buttonUpPin = 12;
const int buttonDownPin = 10;
const int mirrorActivePin = 11;

const char ADC_READ = 'a';
const char BUFFER_AVG_READ = 'b';
const char SHUTTER_CLOSE = 'c';
const char MIRROR_DOWN = 'd';
const char MIRROR_CORRECT_POS = 'e';
const char SAMPLE_SHUTTER_OPEN = 'f';
const char SAMPLE_SHUTTER_CLOSE = 'g';
const char MIRROR_INCORRECT_POS = 'i';
const char MIRROR_MOVING = 'm';
const char SHUTTER_OPEN = 'o';
const char MIRROR_RESET = 'r';
const char MIRROR_STATE = 's';
const char MIRROR_UP = 'u';


const int BUTTON_TIMER_SET= 15000;
int buttonTimer = 0;
bool activeTimer = false;
char mirrorState = MIRROR_CORRECT_POS;

void press_mirror_button(const int buttonPin) {
  pinMode(buttonPin, OUTPUT);
  digitalWrite(buttonPin, LOW);
}
void release_mirror_button(const int buttonPin) {
  pinMode(buttonPin, INPUT);
}
void move_mirror(const char dir, const int buttonPin) {  
    if (mirrorState == MIRROR_CORRECT_POS) {
      press_mirror_button(buttonPin);
      Serial.println(dir);
      buttonTimer = BUTTON_TIMER_SET;
      activeTimer = false;
      mirrorState = MIRROR_INCORRECT_POS;
    }
    else {
      Serial.println(mirrorState);
    }
}
void update_timer() {
  if (buttonTimer > 0) {
    buttonTimer--;
    
    if (buttonTimer == 0) {
      if (activeTimer == false) { //Checking if mirror moving
        if (digitalRead(mirrorActivePin) == 1){
          mirrorState = MIRROR_MOVING;
          activeTimer = true;
          buttonTimer = BUTTON_TIMER_SET;
        }
        else {
          mirrorState = MIRROR_INCORRECT_POS;
        }
        release_mirror_button(buttonUpPin);
        release_mirror_button(buttonDownPin);
      }
      else { //Checking if mirror stopped moving
        if (digitalRead(mirrorActivePin) == 0){
          if (mirrorState == MIRROR_MOVING){
            mirrorState = MIRROR_CORRECT_POS;
          }
          else {
            mirrorState = MIRROR_INCORRECT_POS;
          }
        }
        else {
          buttonTimer = BUTTON_TIMER_SET;
        }
      }
    }
  }
}

void setup() {
  Serial.begin(115200);
  analogReadResolution(12);

  pinMode(shutterPin, OUTPUT);
  digitalWrite(shutterPin, LOW);
  
  pinMode(buttonUpPin, INPUT);
  pinMode(buttonDownPin, INPUT);
  pinMode(mirrorActivePin, INPUT);

  shutter_servo.attach(shutterServoPin);
}

void loop() {
  ringPos++;
  if (ringPos >= BUFSIZE) {
    ringPos = 0;
  }
  adcRingBuffer[ringPos] = analogRead(adcReadPin);
  
  update_timer();
  
  if (Serial.available() > 0) {
    int incomingByte = Serial.read();

    switch (incomingByte) {
      case ADC_READ:
        Serial.println(adcRingBuffer[ringPos]);
        break;
      case BUFFER_AVG_READ:
      {
        unsigned int adcRead = 0;
        for (unsigned int i = 0; i < BUFSIZE; i++) {
          adcRead += adcRingBuffer[i];
        }
        Serial.println(adcRead);
        break;
      }

      case SHUTTER_OPEN:
        digitalWrite(shutterPin, HIGH);
          Serial.println('d');
        break;
      case SHUTTER_CLOSE:
        digitalWrite(shutterPin, LOW);
          Serial.println('d');
        break;
        
      case SAMPLE_SHUTTER_OPEN:
        shutter_servo.write(90);
        Serial.println('O');
        break;
      case SAMPLE_SHUTTER_CLOSE:
        shutter_servo.write(0);
        Serial.println('C');
        break;
        
      case MIRROR_UP:
        move_mirror(MIRROR_UP, buttonUpPin);
        break;
      case MIRROR_DOWN:
        move_mirror(MIRROR_DOWN, buttonDownPin);
        break;
      case MIRROR_STATE:
        Serial.println(mirrorState);
        break;
      case MIRROR_RESET:
        mirrorState = MIRROR_CORRECT_POS;
        break;
        
      default:
        break;
    }
  }
}
