# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import serial
import time
import logging

from .laser_source import *

class OpoChameleonCompact(LaserSource):
    def __init__(self, serial_port):        
        self.ser = serial.Serial(serial_port, baudrate=115200, timeout=1, parity=serial.PARITY_NONE, bytesize=8, stopbits=1)
        logging.debug("OpoChameleonCompact serial: " + self.ser.name)
        self.ser.read_all()
        
        super().__init__()
        
        self.power_w = 0
        self.pump_wavelength_nm = 0
        self.pump_bandwidth_nm = 0
        self.pump_repetition_rate = 0
        self.wavelength_read_successful = False
        
    def close(self):
        self.ser.close()    

    def get_wavelength_read_successful(self):
        return self.wavelength_read_successful
        
    def print_parameters(self, function=print):
        super().print_parameters()
        lp = 30
        function('PUMP WAVELENGTH: '    .ljust(lp) + str(self.pump_wavelength_nm))
        function('PUMP BANDWIDTH: '     .ljust(lp) + str(self.pump_bandwidth_nm))
        function('PUMP REPRATE: '       .ljust(lp) + str(self.pump_reprate))
        function('PUMP IN SHUTTER: '    .ljust(lp) + str(self.pump_in_shutter_open))
        function('PUMP OUT SHUTTER: '   .ljust(lp) + str(self.pump_out_shutter_open))
        function('BEAM SPLITTER: '      .ljust(lp) + str(self.beam_splitter))
        function('STATUS: '             .ljust(lp) + self.status)
        function('HUMIDITY: '           .ljust(lp) + str(self.humidity))
        function('TEMPERATURE: '        .ljust(lp) + str(self.temperature))
        #    self.ser.write(b'*IDN?\n')
        #    function('*IDN: ' + self.ser.readline().decode()[:-1])
    
    def update_parameters(self):
        """
        Functions to query parameters take 0.03s so this is much faster with
        just one call.
        """
        self.ser.read_all()
        self.ser.write(b'PARAMETER?\n')
        
        param_str = self.ser.readline().decode()[:-1].split(';')
        
        self.wavelength_read_successful = True
        
        try:
            value = float(param_str[0]) * 0.1
            self.wavelength_nm = value
        except:
            self.wavelength_read_successful = False
        try:
            value = float(param_str[1]) * 0.1
            self.bandwidth_nm = value
        except:
            pass
        try:
            value = float(param_str[2]) * 0.001
            self.power_w = value
        except:
            pass
        try:
            value = float(param_str[3]) * 0.1
            self.pump_wavelength_nm = value
        except:
            pass
        try:
            value = float(param_str[4]) * 0.1
            self.pump_bandwidth_nm = value
        except:
            pass
        try:
            value = float(param_str[5])
            self.pump_reprate = value
        except:
            pass
        try:
            value = bool(int(param_str[6]))
            self.pump_in_shutter_open = value
        except:
            pass
        try:
            value = bool(int(param_str[7]))
            self.pump_out_shutter_open = value
        except:
            pass
        try:
            value = bool(int(param_str[8]))
            self.shutter_open = value
        except:
            pass
        try:
            value = bool(int(param_str[9]))
            self.beam_splitter = value
        except:
            pass
        try:
            value = float(param_str[10])
            self.humidity = value
        except:
            pass
        try:
            value = float(param_str[11])
            self.temperature = value
        except:
            pass
        try:
            value = param_str[12]
            self.status = value
        except:
            pass

    def set_wavelength_nm(self, nanometers):
        self.wavelength_target = nanometers
        angstroms = nanometers * 10
        command = "OPO WAVELENGTH=" + str(angstroms) + '\n'
        self.ser.write(command.encode('ascii'))

    def set_shutter(self, set_open=False, check=True):
        self.target_shutter = set_open
        value = 0
        if self.target_shutter == True:
            value = 1
            
        command = "OPO OUT SHUTTER=" + str(value) + '\n'
        self.ser.write(command.encode('ascii'))
        
        if check:
            self.update_parameters()
            value = self.get_shutter()
            logging.info('OPO OUT SHUTTER: ' + str(value))
            return value
        
    def repair(self):
        logging.warn("Reparing wavelength: " + str(self.wavelength_target))
        
        self.update_parameters()
        previous_shutter = self.get_shutter()
        self.set_shutter(False)
        
        previous_wavelength = self.wavelength_target
        self.set_wavelength_nm(1300)
        self.wait_status_ok(repair=False)
        self.set_wavelength_nm(previous_wavelength)
        
        while self.wait_status_ok(repair=False) == False:
            previous_wavelength += 5
            if previous_wavelength > 1300:
                previous_wavelength = 1300
            logging.error("Changing to repair wavelength: " + str(previous_wavelength))
            self.set_wavelength_nm(previous_wavelength)

        self.set_shutter(previous_shutter)        
        
    def wait_status_ok(self, repair=True):
        wait_count = 0
        
        while True:
            time.sleep(1)
            wait_count += 1
            self.update_parameters()
            if self.check_status_ok():
                break
            
            while True:
                time.sleep(1)
                wait_count += 1
                self.update_parameters()
                if self.check_status_ok():
                    break
            
                if wait_count > 60:
                    if repair:
                        self.repair()
                        repair = False
                    else:
                        return False
                    
            time.sleep(3)
            wait_count += 3
        return True
        