# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import time
import logging
import serial
import threading

from .photon_counter import *

class PhotonCounterBeckerHickl(PhotonCounter):
    def __init__(self, port):
        self.ser = serial.Serial(port, timeout=20)
        logging.debug("PhotonCounterBeckerHickl serial: " + self.ser.name)
        self.ser.read_all()
        self.mutex = threading.Lock()
        
        self.set_measurement_time(1.0)
        self.set_averaging(1)
        
    def close(self):
        self.ser.close()
        
    def __ask(self, command):
        with self.mutex:
            self.ser.read_all()
            self.ser.write(command.encode('ascii'))
            self.ser.flush()
            return self.ser.readline().decode()
        
    def set_measurement_time(self, seconds):
        super().set_measurement_time(seconds)
        
        ret = self.__ask("SMT " + str(self.measure_time) + "\n").strip('\n')
        if float(ret) == self.measure_time:
            return True
        else:
            return False
        
    def set_averaging(self, averages):
        super().set_averaging(averages)
        
        ret = self.__ask("SAV " + str(self.averages) + "\n").strip('\n')
        if int(ret) == self.averages:
            return True
        else:
            return False

    def print_parameters(self, function=print):
        ret = self.__ask("PP\n")
        ret.strip('\n')
        ret = ret.replace('\t', '\n')
        function(ret)
        
    def measure(self):
        ret = self.__ask("MEAS\n")
        spl = ret.split(' ')
        a = -1
        b = -1
        
        if len(spl) > 3:
            if spl[0] == 'M0:':
                a = float(spl[1])
            if spl[2] == 'M1:':
                b = float(spl[3])
        else:
            logging.error("Invalid response: " + ret)
        
        return (a, b)
