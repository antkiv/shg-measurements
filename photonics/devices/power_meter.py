# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

INVALID_POWER = 9999999

class PowerMeter():
    def close(self):
        pass

    def set_wavelength_nm(self, wavelength_nanometers):
        self.wavelength_nanometers = wavelength_nanometers
        
    def get_power(self, wait=True):
        return INVALID_POWER
