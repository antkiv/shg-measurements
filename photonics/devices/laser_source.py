# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging

class LaserSource:
    def __init__(self):
        self.status = "OK"
        self.target_wavelength_nm   = 0
        self.wavelength_nm          = 0
        self.bandwidth_nm           = 0
        self.power_w                = 0
        self.shutter_open           = False
        self.target_shutter         = False
        
        
        self.set_shutter(False, check=False)
        
        # Get current wavelength and set the same wavelength rounded to integer value.
        self.update_parameters()
        self.set_wavelength_nm(float(int(self.get_wavelength_nm() + 0.5 )))

    def close(self):
        pass
    
    def print_parameters(self, function=print):
        lp=30
        function('STATUS: '             .ljust(lp) + self.status)
        function('TARGET WAVELENGTH: '  .ljust(lp) + str(self.target_wavelength_nm))
        function('WAVELENGTH: '         .ljust(lp) + str(self.wavelength_nm))
        function('BANDWIDTH: '          .ljust(lp) + str(self.bandwidth_nm))
        function('POWER: '              .ljust(lp) + str(self.power_w))
        function('SHUTTER: '            .ljust(lp) + str(self.shutter_open))
        
    
    def update_parameters(self):
        """
        Updates values for getter functions. Avoids polling laser source multiple
        times if parameters are requested multiple times.
        """
        pass
    
    def set_wavelength_nm(self, nanometers):
        self.target_wavelength_nm = nanometers
        self.wavelength_nm = nanometers
        pass

    def set_shutter(self, set_open=False, check=True):
        self.target_shutter = set_open
        self.shutter_open   = set_open
        return self.shutter_open
        
    def repair(self):
        pass
        
    def get_status(self):
        return self.status
    def check_status_ok(self):
        return self.get_status() == "OK"
    def wait_status_ok(self, repair=True):
        self.status = "OK"
        return True
    
    def get_target_wavelength_nm(self):
        return self.target_wavelength_nm
    def get_wavelength_nm(self):
        return self.wavelength_nm     
    def get_bandwidth_nm(self):
        return self.bandwidth_nm    
    def get_power(self):
        return self.power_w
    def get_shutter(self):
        return self.shutter_open
    def get_target_shutter(self):
        return self.target_shutter
