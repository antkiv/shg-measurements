# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import serial

class OscilloscopeTDS200:
    def __init__(self, serial_port):
        self.ser = serial.Serial(serial_port, baudrate=9600, timeout=1, parity=serial.PARITY_NONE, bytesize=8, stopbits=1)
        print("OscilloscopeTDS200 serial: " + self.ser.name)
        
        self.ser.write(b'MEASUREMENT:IMMED:SOURCE CH2 \n')
        self.ser.write(b'MEASUREMENT:IMMED:TYPE MEAN \n')
        #self.ser.write(b'MEASUREMENT:IMMED:TYPE CRMs \n')
        self.ser.write(b'MEASUREMENT:IMMED? \n')
        print(self.ser.readline())
        
    #slow ~0.5s
    def get_measurement(self):
        self.ser.write(b'MEASUREMENT:IMMED:VALUE? \n')
        meas = str(self.ser.readline())
        return float(meas.split(' ')[1][:-3])
        
    def close(self):
        self.ser.close()    

class OscilloscopeTDS3000:
    def __init__(self, serial_port):
        self.ser = serial.Serial(serial_port, baudrate=38400, timeout=1, parity=serial.PARITY_NONE, bytesize=8, stopbits=1)
        #print(self.ser.name)

        self.ser.write(b'DATA:DESTINATION REF1 \n')
        self.ser.write(b'DATA:ENCDG ASCII \n')
        self.ser.write(b'DATA:SOURCE CH2 \n')
        self.ser.write(b'DATA:START 1 \n')
        self.ser.write(b'DATA:STOP 1000 \n')
        self.ser.write(b'DATA:WIDTH 2 \n')
        #self.ser.write(b'DATA? \n')
        #print(self.ser.readline())
        
    def get_measurement(self):
        self.ser.write(b'MEASUREMENT:MEAS3:VALUE? \n')
        meas = str(self.ser.readline())
        return float(meas[2:][:-3])
        
    def get_curve(self):
        self.ser.write(b'CURVE? \n')
        curve = str(self.ser.readline())
        curve = curve[2:]
        curve = curve[:-3]
        curve = curve.split(',')
        return curve
        
    def close(self):
        self.ser.close()    
