# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

class PhotonCounter:
    def __init__(self):
        pass
    
    def close(self):
        pass
    
    def set_measurement_time(self, seconds):
        self.measure_time = float(seconds)
        
    def set_averaging(self, averages):
        self.averages = int(averages)
        
    def get_measurement_time(self):
        return self.measure_time
    
    def get_averaging(self):
        return self.averages
    
    def print_parameters(self, function=print):
        function(self.measure_time)
        function(self.averages)
        
    def measure(self, channel_count=1):
        """
        Counts photons for measurement time and averages values. Multiple channels
        are returned as a tuple.
        """
        if channel_count == 1:
            return 50
        else:
            return (60, 70)
    