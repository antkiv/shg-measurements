# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import serial
import time
import logging
import threading

class MotorController:
    def __init__(self):
        pass

    def close(self):
        pass
        
class Motor:
    def __init__(self, motor_controller, motor_id):
        self.motor_controller = motor_controller
        self.motor_id = motor_id
        
    def set_enable(self, value=True):
        pass
    def get_enabled(self):
        return True
    
    def set_position(self, position, relative=False, wait=True,
                     print_position=True, correct=True):
        pass
            
    def wait_to_stop(self):
        pass
        
    def correct_errors(self):
        pass
        
    def get_position(self):
        return 0.0
                
    def print_position(self):
        print("Motor " + str(self.motor_id) + " at: " +
              str(self.get_position()) )