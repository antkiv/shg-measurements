# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import serial
import time
import logging
import threading

from .motor import *

class MotorControllerNewport(MotorController):
    def __init__(self, serial_port, baud_rate=19200):
        self.ser = serial.Serial(serial_port,
                                 baudrate=baud_rate,
                                 timeout=1,
                                 parity=serial.PARITY_NONE,
                                 bytesize=8,
                                 stopbits=1,
                                 rtscts=True)
        logging.debug("MotorControllerNewport serial: " + self.ser.name)
        self.ser.read_all()
        self.mutex = threading.Lock()
        self.motor = []
        
    def append_motor(self, number):
        self.motor.append(MotorNewport(self, number))

    def close(self):
        self.ser.close()
        
    def write(self, command):
        with self.mutex:
            self.ser.write(command.encode('ascii'))
            
        
    def ask(self, command):
        ret = ""
        with self.mutex:
            self.ser.read_all()
            self.ser.write(command.encode('ascii'))
            ret = self.ser.readline().decode()
        return ret

class MotorNewport(Motor):
    def __init__(self, motor_controller, motor_id):
        super().__init__(motor_controller, motor_id)
        self.__number = str(motor_id)
        
    def set_position(self, position, relative=False, wait=True,
                     print_position=True, correct=True):
        if relative:
            command = "PR" + str(position)
        else:
            command = "PA" + str(position)

        self.write(command)
        
        if wait:
            self.wait_to_stop()
        if correct:
            for i in range(5):
                if self.correct_errors() == True:
                    if i == 4:
                        raise Exception("Could not correct motor errors")
                else:
                    break
        if print_position:
            self.print_position()
            
    def wait_to_stop(self):
        while self.ask("MD?").strip() == "0":
            time.sleep(0.01)
        
    def correct_errors(self):
        corrected = False
        delta = 0.2
        
        for i in range(6):
            if self.ask("MO?").strip() == "0": #motor is off
                self.write("MO") #turn motor on
                corrected = True
                logging.warning("Motor was off. Turned it on.")
                
                self.set_position(delta, relative=True, correct=False)
                delta = -2 * delta
                time.sleep(0.1)
            else:
                break
        return corrected
        
    def get_position(self):
        for i in range(3):
            pos_str = ""
            try:
                pos_str = self.ask("TP").strip()
                return float(pos_str)
            except ValueError:
                logging.warning("Motor position " + pos_str + " is not a number")

        
    def write(self, command_without_motor_prefix):
        command = self.__number + command_without_motor_prefix + '\r'
        self.motor_controller.write(command)
    def ask(self, command_without_motor_prefix):
        command = self.__number + command_without_motor_prefix + '\r'
        return self.motor_controller.ask(command)
