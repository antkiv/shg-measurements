# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import time

try:
    import usbtmc
except:
    print("Exception while importing usbtmc. Install python-ustbtmc using pip.")
    
from .power_meter import *

class PowerMeterThorlabs(PowerMeter):
    def __init__(self, vendor_id = 0x1313, product_id = 0x8078):
        self.inst = usbtmc.Instrument(vendor_id, product_id)
        logging.info(self.inst.ask("*IDN?"))

    def close(self):
        self.inst.close()

    def set_wavelength_nm(self, wavelength_nanometers):
        self.inst.write("SENS:CORR:WAV " + str(wavelength_nanometers))
        logging.info("Thorlabs power meter wavelength:" + 
                     self.inst.ask("SENS:CORR:WAV?"))
        
    def get_power(self, wait=True):
        if wait == False:
            for i in range(4):
                power_str = self.inst.ask("READ?")
    
                try:
                    power = float(power_str)
                except ValueError:
                    logging.warn(str(power_str) + " is not a number")
                    time.sleep(0.5)
                    continue
                
                return power
            return INVALID_POWER
            
        last_power = self.get_power(wait=False)
        
        while True:
            time.sleep(1)
                
            power = self.get_power(wait=False)
            if power == 9.91*10**37: #meter not ready
                continue
            
            if ( abs(1 - last_power / power) < 0.01 ):
                return power
            else:
                last_power = power

#try:
#    import usbtmc
#except ImportError:
#    class PowerMeterThorlabs(PowerMeter):
#        def __init__(self, device="/dev/usbtmc1"):
#            self.device = device
#            self.FILE = os.open(device, os.O_RDWR)
#     
#        def write(self, command):
#            os.write(self.FILE, (command + '\n').encode('ascii'));
#     
#        def read(self, length=None):
#            if length is None:
#                length = 4000
#            return os.read(self.FILE, length)
#    
#        def ask(self, command, length=None):
#            self.write(command)
#            return self.read(length=length).decode('ascii')
#    
#        def ask_for_value(self, command):
#            return eval(self.ask(command).strip())
#     
#        def getName(self):
#            return self.ask("*IDN?")
#     
#        def sendReset(self):
#            self.write("*RST")
#else:
