# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import serial
import time
import logging

from .power_meter import *

class PowerMeterOphir(PowerMeter):
    def __init__(self, serial_port):
        self.ser = serial.Serial(serial_port, baudrate=9600, timeout=1, parity=serial.PARITY_NONE, bytesize=8, stopbits=1)
        logging.debug("PowerMeterOphir serial: " + self.ser.name)

    def close(self):
        self.ser.close()

    def set_wavelength_nm(self, wavelength_nanometers):
        wl = str(int(wavelength_nanometers + 0.5))
        command = ("$WL " + wl + "\r").encode('ascii')
        self.ser.write(command)
        rval = self.ser.readline().decode()
        try:
            if rval[0] == '*':
                logging.info("Ophir power meter wavelength: " + wl)
            else:
                logging.error("Ophir power meter wavelength " + wl + " not set!")
        except Exception:
            logging.error("Ophir power meter wavelength " + wl + " not set!")
    
    def get_power(self, wait=True):
        if wait == False:
            for i in range(4):
                self.ser.write(b'$SP\r')
                value = self.ser.readline().decode()
                power_str = value[1:-2]
    
                try:
                    power = float( power_str )
                except ValueError:
                    logging.warning(str(value) + ", " + str(power_str) + " is not a number")
                    time.sleep(1)
                    continue
                
                return power
            return INVALID_POWER
            
        last_power = self.get_power(wait=False)
        
        while True:
            time.sleep(1.5)
            power = self.get_power(wait=False)
            try:
                if ( abs(1 - last_power / power) < 0.01 ):
                    return power
                else:
                    last_power = power
            except Exception:
                pass
