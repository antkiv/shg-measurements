# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import serial
import time
import logging
import threading
import ctypes

from .motor import * 

class MotorControllerThorlabs(MotorController):
    """
    Supported controllers:
        DC101
        BPC203
        BSC103
    """
    def __init__(self, serial_port, controller="", baud_rate=115200):
        self._ser = serial.Serial(serial_port,
                                 baudrate=baud_rate,
                                 timeout=1,
                                 parity=serial.PARITY_NONE,
                                 bytesize=8,
                                 stopbits=1,
                                 rtscts=True)
        logging.debug("MotorControllerThorlabs serial: " + self._ser.name)
        self._mutex = threading.Lock()
        
        # Initialize communications
        time.sleep(0.050)
        self._ser.reset_input_buffer()
        self._ser.reset_output_buffer()
        time.sleep(0.050)
        self._ser.setRTS(True)
        
        self._source    = b"\x01"
        self._dest      = b"\x11"
        self.controller = controller
        if self.controller == "KDC101":
            self._dest     = b"\x50"
        
        self.stop_update_messages()
        time.sleep(1)
        print(self._ser.read_all())
        print("Hw_info: ", self.req_hw_info())
    
        #get all motors to a list
        self.motor = []
        for i in range(9):
            new_motor = None
            if self.controller == "KDC101":
                new_motor = MotorThorlabs(self, b"\x50")
            else:
                if self.is_rack_bay_used(i):
                    dest = (b"\x20"[0] | (i+1).to_bytes(1, byteorder="little")[0])
                    dest = dest.to_bytes(1, byteorder="little")
                    if self.controller == "BPC203":
                        new_motor = PiezoMotorThorlabs(self, dest)
                    elif self.controller == "BSC103":
                        new_motor = MotorThorlabs(self, dest)
                else:
                    break
                
            new_motor.no_flash_programing()
            new_motor.set_enable()
            if self.controller == "BSC103":
                new_motor.set_limit_switches()
                new_motor.set_velocity_acceleration(-500000, 500000, 300000)
                new_motor.set_power_move_rest_percentage(100,5)
                
            self.motor.append(new_motor)
            if self.controller == "KDC101":
                break
                
    def close(self):
        with self.mutex:
            self._ser.close()

    def _write(self, command):
        with self._mutex:
            self._ser.write(command)
    def _ask(self, command, byte_count, mutex=True):
        if mutex:
            with self._mutex:
                read = self._ser.read_all()
                if len(read) > 0:
                    print(read)
                    
                self._ser.write(command)
                return self._ser.read(byte_count)
        else:
            self._ser.write(command)
            return self._ser.read(byte_count)
        
    def req_hw_info(self):
        #MGMSG_HW_REQ_INFO
        header = b"\x05\x00\x00\x00" + self._dest + self._source
        ret = self._ask(header, 90)
        
        header = ret[0:6].hex()
        serial = int.from_bytes(ret[6:10], byteorder='little', signed=False)
        model = ret[10:18].decode('ascii')
        controller_type = ret[18:20].hex()
        firmware = ret[20:24].hex()
        notes = ret[24:72].decode('ascii')
        empty = ret[72:84].decode('ascii')
        hardware = ret[84:86].hex()
        mod_state = ret[86:88].hex()
        channel_count = int.from_bytes(ret[88:90], byteorder='little', signed=False)
        
        return header, serial, model, controller_type, firmware, \
               notes, empty, hardware, mod_state, channel_count      
    def is_rack_bay_used(self, bay):
        #MGMSG_RACK_REQ_BAYUSED
        bay = bay.to_bytes(1, byteorder="little")
        header = b"\x60\x00" + bay + b"\x00" + self._dest + self._source
        ret = self._ask(header, 6)
        try:
            state = int(ret[3])
            if state == 1:
                return True
            else:
                return False
        except:
            return False
    def is_hub_bay_used(self, bay):
        #MGMSG_HUB_REQ_BAYUSED
        header = b"\x65\x00\x00\x00" + self._dest + self._source
        ret = self._ask(header, 6)
        print(ret)
        try:
            return int.from_bytes(ret[2], byteorder='little', signed=True)
        except:
             return -2
    def stop_update_messages(self):
        #MGMSG_HW_STOP_UPDATEMSGS
        header = b"\x12\x00\x00\x00" + self._dest + self._source
        self._write(header) 
    def start_update_messages(self):
        #MGMSG_HW_START_UPDATEMSGS
        header = b"\x11\x00\x01\x00" + self._dest + self._source
        self._write(header)
        
    def set_zero_offsets(self):
        for m in self.motor:
            if self.controller == "BPC203":
                m.set_zero_offset()
    def zero_stages(self):
        for m in self.motor:
            if self.controller == "BPC203":
                m.zero_stage()
        
        
        
class MotorThorlabs(Motor):
    def __init__(self, motor_controller, motor_id):
        self.motor_controller  = motor_controller
        
        self._chan_ident_word  = b"\x01\x00"
        self._chan_ident_byte  = b"\x01"
        self._source           = b"\x01"
        self._dest             = motor_id
        self._dest_pipe        = (motor_id[0] | b"\x80"[0]).to_bytes(1, byteorder='little')
        self.set_pos_vel_acc_params()
        
        self.no_flash_programing()
        
    def set_enable(self, value=True):
        byte = b"\x02"
        if value == True:
            byte = b"\x01"
        #MGMSG_MOD_SET_CHANENABLESTATE
        header = b"\x10\x02" + self._chan_ident_byte + byte + self._dest + self._source
        self.motor_controller._write(header)
    def get_enabled(self):
        #MGMSG_MOD_REQ_CHANENABLESTATE
        header = b"\x11\x02" + self._chan_ident_byte + b"\x00" + self._dest + self._source
        if self.motor_controller._ask(header, 6)[3] == 1:
            return True
        return False
             
    def set_position(self, position, relative=False,
                     wait=False, print_position=False, correct=False):
        if relative:
            #MGMSG_MOT_MOVE_RELATIVE
            header = b"\x48\x04\x06\x00" + self._dest_pipe + self._source
        else:
            #MGMSG_MOT_MOVE_ABSOLUTE
            header = b"\x53\x04\x06\x00" + self._dest_pipe + self._source
            
        pos     = ctypes.c_int32(int(position * self.position_scale))
        command = header + self._chan_ident_word + pos
        self.motor_controller._write(command)
        
        if wait:
            self.wait_to_stop()
        if correct:
            pass
        if print_position:
            self.print_position()
            
    def get_position(self):
        #MGMSG_MOT_REQ_POSCOUNTER
        header = b"\x11\x04" + self._chan_ident_byte + b"\x00" + self._dest + self._source
        rv     = self.motor_controller._ask(header, 16)
        return int.from_bytes(rv[-4:], byteorder='little', signed=True) / self.position_scale
        
            
    def wait_to_stop(self, wait_time_step=0.1):
        #MGMSG_MOT_MOVE_COMPLETED
        header      = b"\x64\x04" + self._chan_ident_byte + b"\x00" + self._dest_pipe + self._source
        with self.motor_controller._mutex:
            while (self.motor_controller._ser.read(6) != header):
                time.sleep(wait_time_step)
    def correct_errors(self):
        pass    
        
    def set_pos_vel_acc_params(self, pos=1, vel=1, acc=1):
        self.position_scale = pos
        self.velocity_scale = vel
        self.acceleration_scale = acc        
    def set_stage(self, name):
        if name == "PRM1-Z8":
            self.set_pos_vel_acc_params(1919.64, 42941.66, 14.66)
        if name == "DRV-001":
            self.set_pos_vel_acc_params(51200, 51200, 51200)
            
            
    def identify_flash_led(self):
        #MGMSG_MOD_IDENTIFY
        header = b"\x23\x02\x00\x00" + self._dest + self._source
        self.motor_controller._write(header) 
    def no_flash_programing(self): #must be sent at start according to manual
        #MGMSG_HW_NO_FLASH_PROGRAMMING
        header = b"\x18\x00\x00\x00" + self._dest + self._source
        self.motor_controller._write(header) 
        
    def set_velocity_acceleration(self, min_velocity, max_velocity, acceleration):
        #MGMSG_MOT_SET_VELPARAMS
        header      = b"\x13\x04\x0E\x00" + self._dest_pipe + self._source
        min_vel     = ctypes.c_uint32(min_velocity) #negative if moving other direction
        acc         = ctypes.c_uint32(acceleration)
        max_vel     = ctypes.c_uint32(max_velocity)
    
        command = header + self._chan_ident_word + min_vel + acc + max_vel
        self.motor_controller._write(command)
    def set_limit_switches(self):
        #MGMSG_MOT_SET_LIMSWITCHPARAMS
        header      = b"\x23\x04\x10\x00" + self._dest_pipe + self._source
        cw_hard     = b"\x01\x00" #ignore
        ccw_hard    = b"\x01\x00" #ignore
        cw_soft     = ctypes.c_int32(480000)
        ccw_soft    = ctypes.c_int32(0)
        mode        = b"\x03\x00" #profiled soft stop
    
        command = header + self._chan_ident_word + cw_hard + ccw_hard + cw_soft + ccw_soft + mode
        self.motor_controller._write(command)
    def set_power_move_rest_percentage(self, move_power, rest_power):
        #MGMSG_MOT_SET_POWERPARAMS
        header      = b"\x26\x04\x06\x00" + self._dest_pipe + self._source
        move_power  = min(max(int(move_power), 0), 100)
        rest_power  = min(max(int(rest_power), 0), 100)
        move_power  = ctypes.c_uint16(move_power)
        rest_power  = ctypes.c_uint16(rest_power)
    
        command = header + self._chan_ident_word + rest_power + move_power
        self.motor_controller._write(command)
    def home(self, direction_positive=False, speed=51200):
        #MGMSG_MOT_SET_HOMEPARAMS
        header       = b"\x40\x04\x0E\x00" + self._dest_pipe + self._source
        if direction_positive:
            direction    = b"\x01\x00"
            limit_switch = b"\x04\x00"
            velocity     = ctypes.c_int32(-speed)
            offset       = ctypes.c_int32(0)        
        else:
            direction    = b"\x02\x00"
            limit_switch = b"\x01\x00"
            velocity     = ctypes.c_int32(speed)
            offset       = ctypes.c_int32(0)   
    
        command = header + self._chan_ident_word + direction + limit_switch + velocity + offset
        self.motor_controller._write(command)
        
        #MGMSG_MOT_MOVE_HOME
        header = b"\x43\x04" + self._chan_ident_byte + b"\x00" + self._dest + self._source
        self.motor_controller._write(header)

class PiezoMotorThorlabs(MotorThorlabs):
    def __init__(self, motor_controller, motor_id):
        super().__init__(motor_controller, motor_id)
        self.set_loop_mode()
        self.set_position_value = 0
        
    def set_loop_mode(self, closed=True, smooth=True):
        mode = b"\x01"
        if closed:
            if smooth:
                mode = b"\x04"
            else:                
                mode = b"\x02"                
        else:
            if smooth:
                mode = b"\x03"
            else:
                mode = b"\x01"
        
        #MGMSG_PZ_SET_POSCONTROLMODE
        header      = b"\x40\x06" + self._chan_ident_byte + mode + self._dest + self._source
        self.motor_controller._write(header)

    def set_position(self, position, relative=False,
                     wait_time=1.0, wait_tolerance=2,
                     print_position=False, correct=False):
        if relative:
            self.set_position_value += position
        else:
            self.set_position_value  = position
        
        
        #MGMSG_PZ_SET_OUTPUTPOS
        header    = b"\x46\x06\x04\x00" + self._dest_pipe + self._source
        pos_int   = int(self.set_position_value * self.position_scale)
        # Clamp to range of int16
        max_value = 32767
        min_value = -32768
        pos_int   = min(pos_int, max_value)
        pos_int   = max(pos_int, min_value)
        pos_short = ctypes.c_int16(pos_int)
        command   = header + self._chan_ident_word + pos_short
        
        with self.motor_controller._mutex:
            self.motor_controller._ser.write(command)
            
            if wait_time > 0.0:
                start = time.perf_counter()
                while ((time.perf_counter() - start) < wait_time):
                    get_pos = self.get_position(mutex=False)
                    if (abs(get_pos - pos_int) < wait_tolerance):
                        break
            if print_position:
                print(self.get_position(mutex=False))
        if correct:
            pass
            
        
    def get_position(self, mutex=True):
        #MGMSG_PZ_REQ_OUTPUTPOS
        header  = b"\x47\x06" + self._chan_ident_byte + b"\x00" + self._dest + self._source
        rv      = self.motor_controller._ask(header, 10, mutex)
        rv_int  = int.from_bytes(rv[-2:], byteorder='little', signed=True)
        return (rv_int - self.zero_position_offset) / self.position_scale
    
    
    def set_pi_constants(self, proportional, integral):        
        #MGMSG_PZ_SET_PICONSTS
        header    = b"\x55\x06\x06\x00" + self._dest_pipe + self._source
        proportional_byte = ctypes.c_uint8(proportional)
        integral_byte     = ctypes.c_uint8(integral)
        command   = header + self._chan_ident_word + proportional_byte + b"\x00" + integral_byte + b"\x00"
        
        self.motor_controller._ser.write(command)
        
    def get_pi_constants(self, mutex=True):
        #MGMSG_PZ_REQ_PICONSTS
        header  = b"\x56\x06" + self._chan_ident_byte + b"\x00" + self._dest + self._source
        rv      = self.motor_controller._ask(header, 12, mutex)
        proportional = int.from_bytes(rv[-4:-2], byteorder='little', signed=True)
        integral     = int.from_bytes(rv[-2:], byteorder='little', signed=True)
        print(rv)
        return (proportional, integral)
        
    def zero_stage(self):
        #MGMSG_PZ_SET_ZERO
        header    = b"\x58\x06" + self._chan_ident_byte + b"\x00" + self._dest + self._source
        self.motor_controller._ser.write(header)
        self.zero_position_offset = 0
        self.zero_position_offset = self.get_position()
        print("Zero position offset", self.zero_position_offset)
        
    def set_zero_offset(self):
        self.set_position(0, relative=False, wait_time=0)
        time.sleep(2.0)
        self.zero_position_offset = 0
        self.zero_position_offset = self.get_position()
        print("Zero position offset", self.zero_position_offset)
        
    
    
if __name__ == "__main__":
    try:
        mc.close()
    except:
        pass
    mc = MotorControllerThorlabs("/dev/ttyUSB0", "BSC103")
    m = mc.motor[0]
    #m.set_stage("PRM1-Z8")
    m.set_stage("DRV-001")
    m.set_enable()
    m.get_position()
