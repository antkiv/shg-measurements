# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import logging
import pickle
import matplotlib.pyplot as plt
import numpy as np

class NoneClass: 
    """
    Placeholder class for testing and unimplemented classes.
    """
    def __getattr__(self, name):
#        print("NoneClass getattr:", name)
        return self
    def __call__(self, *args, **kwargs):
        pass
#        print("NoneClass call")

def maximize_plot():
    """
    Maximizes matplotlib plots. Works on QT5 backend. May need to reimplement
    for others.
    """
    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()
    figManager.window.showMinimized()

def get_byte_sizeof(obj):
    """
    Returns size of object in bytes
    """
    p = pickle.dumps(obj)
    return len(p)

def sci_not(x):
    """
    Convert number to scientific notation string with 4 digits
    """
    return '{:.3e}'.format(x)

def print_var_info(name, x):
    print( name.ljust(30) + sci_not(x) )
def print_list_info(name, x):
    print( name.ljust(30) + sci_not(np.min(x)) + " to " + sci_not(np.max(x)) + " shape: " + str(x.shape) )

def log1abs(x):
    return np.log(1 + np.abs(x))
        
def check_exists_fix_filename(filename):
    """
    New filename, that is not in the working directory, is returned if filename
    exists in the working directory. Otherwise the original filename is returned.
    """
    new_filename = filename
    i = 0
    while os.path.isfile(new_filename):
        i = i + 1
        new_filename = str(i) + "-" + filename
    if i != 0:
        logging.warn("File already exists. Renaming to " + new_filename)
    return new_filename

def get_files_in_directory(directory = "."):
    """
    Returns a list of files in a directory.
    """
    for root, root_dirs, data_files in os.walk(directory):
        break
    return data_files

try:
    import winsound
except ImportError:
    def playsound(frequency,duration_ms):
        """
        Not implemented.
        """
        pass
        #apt-get install beep
        #os.system('beep -f %s -l %s' % (frequency,duration_ms))
        #os.system('(speaker-test -t sine -f %s)& pid=$!; sleep %s; kill -9 $pid ' % (frequency,duration_ms*0.001))

else:
    def playsound(frequency,duration_ms):
        """
        Plays a beep in Windows.
        """
        winsound.Beep(frequency,duration_ms)