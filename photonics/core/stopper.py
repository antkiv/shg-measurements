# Copyright 2019 Antti Kiviniemi
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
A module for starting and stopping a function. 
"""

import threading
import logging
import socket

_running = False
_thread = None
_bt_thread = None
_run_bt_server = False

def start_bt_server(host_mac_address="00:19:0E:19:5F:0E"):
    """
    Starts a bluetooth server that returns the running status of a function
    started with start_function()
    """
    global _run_bt_server, _bt_thread
    
    if _run_bt_server:
        logging.warn("Bluetooth server already running.")
        return
    _run_bt_server = True
    
    def run_bluetooth_server():
        port    = 3
        backlog = 1
        size    = 1024
        while _run_bt_server:
            try:
                s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
                s.bind( (host_mac_address,port) )
                s.listen(backlog)
        
                while _run_bt_server:
                    client, address = s.accept()
                    logging.info("Bluetooth client accepted.")
                    
                    try:
                        while _run_bt_server:
                            # Blocks until some data is received
                            data = client.recv(size)
                            
                            # Just return the running state and do not use data.
                            if _running:
                                client.send(str("Running: ").encode('ascii'))
                            else:
                                client.send(str("Stopped").encode('ascii'))
                    except:
                        logging.warn("Exception in bluetooth receive/send")
                        pass
                    client.close()
    
                logging.info("Closing bluetooth socket.")
                s.close()
            except:
                logging.warn("Exception in bluetooth init.")
                pass
            
    _bt_thread = threading.Thread(target=run_bluetooth_server)
    _bt_thread.start()
    logging.info("Bluetooth thread started.")
    
def stop_bl_server():
    global _run_bt_server, _bt_thread
    _run_bt_server = False
    
    logging.info("Stopping bluetooth thread")
    if _bt_thread:
        _bt_thread.join()
    _bt_thread = None
    logging.info("Bluetooth thread stopped.")
    
    
def start_function(function, args=None):
    """
    Starts a function that can be stopped using stop()-function.
    """
    
    global _running, _thread
    
    if _running == False:
        _running = True
        
        def run():
            global _running
            if args:
                function(*args)
            else:
                function()
            _running = False
        
        _thread = threading.Thread(target=run)
        _thread.start()
        logging.info("New thread. To stop use stop()")
    else:
        if threading.main_thread() == threading.current_thread():
            logging.error("""Something already started.
                          Please stop it before starting another function.
                          Start function called from main thread when already running.""")
            return
        if args:
            function(*args)
        else:
            function()
        logging.info("""Start function called from an already started thread.
                     To stop use stop()""")
        
def stop():
    global _running, _thread
    
    _running = False
    if _thread:
        _thread.join()
    _thread = None
    
def loop_function(function, args=None):
    """
    Starts a function (with start_function()) that is called repeatedly until
    stop() is called.
    """
    def loop_continuously(args = None):
        global _running
        while _running:
            if args:
                function(*args)
            else:
                function()

    start_function(loop_continuously, args)

def is_running():
    """
    Returns true if a function is still running (started by start_function).
    """
    return _running
